//
//  State.cpp
//  primitiveApp
//
//  Created by Nikita Kunevich on 2/4/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#include "State.hpp"
#include "Utility.hpp"

namespace primitive
{
    State::State(WorkerPtr worker, ShapePtr shape, int alpha)
    {
        this->worker = worker;
        this->shape = shape;
        this->score = -1;
        
        if (alpha == 0)
        {
            alpha = 128;
            this->mutate_alpha = true;
        }
        
        this->alpha = alpha;
    }
    
    State::State(WorkerPtr worker, ShapePtr shape, int alpha, bool mutate_alpha, double score)
        : worker(worker), shape(shape), alpha(alpha), mutate_alpha(mutate_alpha), score(score)
    {
    }
    
    double State::energy()
    {
        if (score < 0)
        {
            score = worker->energy(shape, alpha);
        }
        
        return score;
    }
    
    
    double State::recomputeEnergy()
    {
        score = worker->energy(shape, alpha);
        return score;
    }
    
    
    AnnealablePtr State::doMove()
    {
        AnnealablePtr old_state = this->copy();
        shape->mutate(worker->width, worker->height);
        score = -1;
        if (this->mutate_alpha)
        {
            alpha = clamp<int>(alpha + arc4random() % 21 - 10, 1, 255);
        }
        //question mark
        return old_state;
    }
    
    void State::undoMove(AnnealablePtr undo)
    {
        auto old_state = std::dynamic_pointer_cast<State>(undo);
        this->shape = old_state->shape;
        this->alpha = old_state->alpha;
        this->score = old_state->score;
    }
    
    AnnealablePtr State::copy()
    {
        return std::make_shared<State>(this->worker, this->shape->copy(), this->alpha, this->mutate_alpha, this->score);
    }
}
