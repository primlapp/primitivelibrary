//
//  CHUtils.h
//  primitiveApp
//
//  Created by Nikita Kunevich on 1/20/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef CHUtils_h
#define CHUtils_h

#include <CoreGraphics/CoreGraphics.h>
#include <vector>
#include "Color.hpp"
#include "Image.hpp"
namespace primitive {
    
    CGContextRef ResizeCGImageAndGetContext(CGImageRef original_img, uint32_t new_width, uint32_t new_height);
    CGImageRef ResizeCGImage(CGImageRef orig_img, uint32_t max_dimension);
    CGContextRef CGDefaultContextRefFromBuffer(uint8_t* buffer, int32_t width, int32_t height);
    
    CGContextRef CGContextFromImageRef(CGImageRef img_ref);
    
    CGContextRef CGEmptyContextFromRect(CGRect rect);
    
    CGImageRef CGImageFromCGContext(CGContextRef context);
    
    void RGBA32ToCGFloatArray(RGBA32Color color, CGFloat* cg_color);
    
    namespace CGDraw
    {
        void DrawTriangle(CGContextRef context, Point a, Point b, Point c, RGBA32Color color);
        void DrawRectangle(CGContextRef context, Point a, Point b, RGBA32Color color);
        void DrawEllipse(CGContextRef context, Point center, Point size, RGBA32Color color);
        void DrawPolygon(CGContextRef context, std::vector<Point> &points, RGBA32Color color);
        void DrawBezier(CGContextRef context, std::vector<Point> &points, RGBA32Color color);
    }
}
#endif /* CHUtils_h */
