#include "CGUtils.hpp"
#include <stdlib.h>

namespace primitive {
    
    CGContextRef CGContextFromImageRef(CGImageRef img_ref) {
        size_t width = CGImageGetWidth(img_ref);
        size_t height = CGImageGetHeight(img_ref);
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        size_t bytesPerPixel = 4;
        size_t bytesPerRow = bytesPerPixel * width;
        size_t bitsPerComponent = 8;
        
        CGContextRef context = CGBitmapContextCreate(NULL, width, height,
                                                     bitsPerComponent, bytesPerRow, colorSpace,
                                                     kCGImageAlphaPremultipliedLast | kCGBitmapByteOrderDefault);
        CGColorSpaceRelease(colorSpace);
        
        CGContextDrawImage(context, CGRectMake(0, 0, width, height), img_ref);
        //        CGContextSetShouldAntialias(context, true);
        return context;
    }
    
    CGContextRef CGDefaultContextRefFromBuffer(uint8_t* buffer, int32_t width, int32_t height) {
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        int bytesPerPixel = 4;
        int bytesPerRow = bytesPerPixel * width;
        int bitsPerComponent = 8;
        
        CGContextRef context = CGBitmapContextCreate(buffer, width, height,
                                                     bitsPerComponent, bytesPerRow, colorSpace,
                                                     kCGImageAlphaPremultipliedLast | kCGBitmapByteOrderDefault);
        CGColorSpaceRelease(colorSpace);
        
        return context;
        
    }
    
    
    CGContextRef ResizeCGImageAndGetContext(CGImageRef original_img, uint32_t new_width, uint32_t new_height) {
        
        int bits_per_component = 8;
        int bytes_per_pixel = 4;
        CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
        
        CGContextRef context = CGBitmapContextCreate(NULL,
                                                     new_width, // Changed this
                                                     new_height, // Changed this
                                                     bits_per_component,
                                                     new_width * bytes_per_pixel,
                                                     colorspace,
                                                     kCGImageAlphaPremultipliedLast | kCGBitmapByteOrderDefault);
        
        CGColorSpaceRelease(colorspace);
        
        if(context == NULL)
            return nil;
        
        //check speed
        //CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
        
                CGContextDrawImage(context, CGContextGetClipBoundingBox(context), original_img);
                CGContextSetShouldAntialias(context, true);
        return context;
        
    }
    
    CGImageRef ResizeCGImage(CGImageRef orig_img, uint32_t max_dimension) {
        size_t width = CGImageGetWidth(orig_img);
        size_t height = CGImageGetHeight(orig_img);
        
        size_t max_img_dimension = std::max(width, height);
        
        if (max_img_dimension > max_dimension)
        {
            //resize
            bool width_bigger = width > height;
            
            CGFloat aspect_ratio = (CGFloat)width / height;
            uint32_t new_width = width_bigger ? max_dimension : aspect_ratio * max_dimension;
            uint32_t new_height = width_bigger ? max_dimension / aspect_ratio : max_dimension;
            
            CGContextRef resized_image_context = ResizeCGImageAndGetContext(orig_img, new_width, new_height);
            return CGImageFromCGContext(resized_image_context);
        }
        else
        {
            CGContextRef context = CGContextFromImageRef(orig_img);
            return CGImageFromCGContext(context);
        }
    }
    
    CGContextRef CGEmptyContextFromRect(CGRect rect)
    {
        int bits_per_component = 8;
        int bytes_per_pixel = 4;
        CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
        CGContextRef context = CGBitmapContextCreate(NULL, rect.size.width,
                                                     rect.size.height,
                                                     bits_per_component,
                                                     rect.size.width * bytes_per_pixel,
                                                     colorspace,
                                                     kCGImageAlphaPremultipliedLast | kCGBitmapByteOrderDefault);
        CGColorSpaceRelease(colorspace);
        return context;
    }
    
    
    void RGBA32ToCGFloatArray(RGBA32Color color, CGFloat* cg_color)
    {
        cg_color[0] = color.r / 255.0;
        cg_color[1] = color.g / 255.0;
        cg_color[2] = color.b / 255.0;
        cg_color[3] = color.a / 255.0;
    }
    
    //fix memory leak
    CGImageRef CGImageFromCGContext(CGContextRef context)
    {
        CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
        size_t width = CGBitmapContextGetWidth(context);
        size_t height = CGBitmapContextGetHeight(context);
        uint8_t* buffer = (uint8_t*)CGBitmapContextGetData(context);
        CFDataRef rgbData = CFDataCreate(NULL, buffer, width * height * 4);
        
        CGDataProviderRef provider = CGDataProviderCreateWithCFData(rgbData);
        CGImageRef rgbImageRef = CGImageCreate(width, height, 8, 32, width * 4, colorspace, kCGBitmapByteOrderDefault | kCGImageAlphaPremultipliedLast, provider, NULL, true, kCGRenderingIntentDefault);
        
        
//        CGImageRef cgImage = CGBitmapContextCreateImage(context);
        
        
        CGDataProviderRelease(provider);
        CFRelease(rgbData);
        CGColorSpaceRelease(colorspace);
        // use the created CGImage
        return rgbImageRef;
    }
    
    namespace CGDraw {
        void DrawTriangle(CGContextRef context, Point a, Point b, Point c, RGBA32Color color)
        {
            
            CGContextBeginPath(context);
            CGContextMoveToPoint   (context, a.x, a.y);
            CGContextAddLineToPoint(context, b.x, b.y);
            CGContextAddLineToPoint(context, c.x, c.y);
            CGContextClosePath(context);
            
            CGContextSetRGBFillColor(context, color.r / 255.0f, color.g / 255.0f, color.b / 255.0f, color.a / 255.0f);
            
            //TODO: fix crash context address invalid
            CGContextFillPath(context);
        }
        
        void DrawRectangle(CGContextRef context, Point a, Point b, RGBA32Color color)
        {
            CGContextBeginPath(context);
            CGContextMoveToPoint   (context, a.x, a.y);
            CGContextAddLineToPoint(context, a.x, b.y);
            CGContextAddLineToPoint(context, b.x, b.y);
            CGContextAddLineToPoint(context, b.x, a.y);
            CGContextClosePath(context);
            
            CGContextSetRGBFillColor(context, color.r / 255.0f, color.g / 255.0f, color.b / 255.0f, color.a / 255.0f);
            CGContextFillPath(context);
        }
        
        
        void DrawEllipse(CGContextRef context, Point center, Point size, RGBA32Color color)
        {
            int32_t x = center.x - size.x / 2;
            int32_t y = center.y - size.y / 2;
            CGContextBeginPath(context);
            CGContextAddEllipseInRect(context, CGRectMake(x, y, size.x, size.y));
            CGContextClosePath(context);
            
            CGContextSetRGBFillColor(context, color.r / 255.0f, color.g / 255.0f, color.b / 255.0f, color.a / 255.0f);
            CGContextFillPath(context);
        }
        
        
        void DrawPolygon(CGContextRef context, std::vector<Point> &points, RGBA32Color color)
        {
            CGContextBeginPath(context);            
            CGContextMoveToPoint(context, points[0].x, points[0].y);
            
            size_t size = points.size();
            for (size_t count = 1; count < size; count++)
            {
                CGContextAddLineToPoint(context, points[count].x, points[count].y);
            }
            
            CGContextClosePath(context);
            
            CGContextSetRGBFillColor(context, color.r / 255.0f, color.g / 255.0f, color.b / 255.0f, color.a / 255.0f);
            CGContextFillPath(context);
        }
        
        void DrawBezier(CGContextRef context, std::vector<Point> &points, RGBA32Color color)
        {
            if (points.size() >= 4)
            {
                CGContextBeginPath(context);
                CGContextMoveToPoint(context, points[0].x, points[0].y);
                
                CGContextAddCurveToPoint(context,
                                         points[1].x, points[1].y,
                                         points[2].x, points[2].y,
                                         points[3].x, points[3].y);
                
                CGContextSetLineWidth(context, 2.0);
                
                CGContextSetRGBStrokeColor(context, color.r / 255.0f, color.g / 255.0f, color.b / 255.0f, color.a / 255.0f);
                CGContextStrokePath(context);
            }
        }
    }
}
