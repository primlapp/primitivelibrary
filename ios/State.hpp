//
//  State.hpp
//  primitiveApp
//
//  Created by Nikita Kunevich on 2/4/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef State_hpp
#define State_hpp

#include "Worker.hpp"
#include "Shape.hpp"
#include "Optimize.hpp"


namespace primitive
{
    
    struct State;
    typedef std::shared_ptr<State> StatePtr;
    
    struct State : Annealable
    {
        WorkerPtr worker;
        ShapePtr shape;
        double score;
        int alpha;
        bool mutate_alpha;
        
        State(WorkerPtr worker, ShapePtr shape, int alpha);
        State(WorkerPtr worker, ShapePtr shape, int alpha, bool mutate_alpha, double score);
        
        double recomputeEnergy();
        
        virtual double energy() override;
        virtual AnnealablePtr doMove() override;
        virtual void undoMove(AnnealablePtr) override;
        virtual AnnealablePtr copy() override;
    };
    
    
}
#endif /* State_hpp */
