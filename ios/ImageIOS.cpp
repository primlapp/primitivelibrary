//
//  ImageIOS.cpp
//  primitiveApp
//
//  Created by Nikita Kunevich on 1/20/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#include "ImageIOS.hpp"
#include "CGUtils.hpp"
#include "Shapes.hpp"

#include <algorithm>
#include <cstdlib>

using namespace std;

namespace primitive
{

    ImageIOS::ImageIOS(CGContextRef context)
    {
        _context = context;
        _managesMem = false;
        width = (uint32_t)CGBitmapContextGetWidth(_context);
        height = (uint32_t)CGBitmapContextGetHeight(_context);
    }
    
    ImageIOS::ImageIOS(CGContextRef context, uint8_t* buffer)
    {
        _context = context;
        _buffer = buffer;
        _managesMem = true;
        width = (uint32_t)CGBitmapContextGetWidth(_context);
        height = (uint32_t)CGBitmapContextGetHeight(_context);
    }
    
    void ImageIOS::fillRect(Point p1, Point p2, RGBA32Color col)
    {
        
        CGContextSaveGState(_context);
        CGFloat cg_color[4];
  
        RGBA32ToCGFloatArray(col, cg_color);
        CGColorSpaceRef cs = CGColorSpaceCreateDeviceRGB();
        CGColorRef color = CGColorCreate(cs, cg_color);
        CGColorSpaceRelease(cs);
        CGContextSetFillColorWithColor(_context, color);
        CGRect rect = CGRectMake(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y);
        CGContextFillRect(_context, rect);
        CGContextRestoreGState(_context);
    }
    
    RGBA32Color ImageIOS::getPixelAt(uint32_t x, uint32_t y)
    {
        size_t offset = (y * width + x) * 4;
        uint8_t* buffer = (uint8_t*)CGBitmapContextGetData(_context);
        RGBA32Color color = {
            buffer[offset],     buffer[offset + 1],
            buffer[offset+2],   buffer[offset+3]
        };
        return color;
    }
    
    RGBA32Color ImageIOS::getPixelAt(Point point)
    {
        return ImageIOS::getPixelAt(point.x, point.y);
    }
    
    void ImageIOS::setPixelAt(Point point, RGBA32Color color)
    {
        uint32_t x = point.x;
        uint32_t y = point.y;
        size_t offset = (y * width + x) * 4;
        uint8_t* buffer = (uint8_t*)CGBitmapContextGetData(_context);
        memcpy(buffer + offset, &color, 4);
    }


    CGImageRef ImageIOS::getImage()
    {
//        return CGImageFromCGContext(_context);
        return CGBitmapContextCreateImage(_context);

    }
    
    ImageIOS::~ImageIOS() {
        CGContextRelease(_context);
        if (_managesMem)
            free(_buffer);
    }

    
    ImagePtr ImageIOS::copy(bool empty)
    {
        size_t size = this->height * this->width * 4;
        uint8_t* buffer = (uint8_t*)malloc(this->height * this->width * 4);
        if (!empty)
            memcpy(buffer, CGBitmapContextGetData(_context), size);
        CGContextRef context = CGDefaultContextRefFromBuffer(buffer, width, height);
        return ImagePtr(new ImageIOS(context, buffer));
    }

    
    Point& ImageIOS::pointToCGCoord(uint32_t height, Point& point)
    {
        point.y = height - point.y;
        return point;
    }
    
    void ImageIOS::drawTriangle(Point a, Point b, Point c, RGBA32Color color)
    {
        a = pointToCGCoord(height, a);
        b = pointToCGCoord(height, b);
        c = pointToCGCoord(height, c);
        CGDraw::DrawTriangle(_context, a, b, c, color);
    }
    
    void ImageIOS::drawRectangle(Point a, Point b, RGBA32Color color)
    {
        CGDraw::DrawRectangle(_context, pointToCGCoord(height, a), pointToCGCoord(height, b), color);
    }
    
    void ImageIOS::drawEllipse(Point center, Point size, RGBA32Color color)
    {
        CGDraw::DrawEllipse(_context, pointToCGCoord(height, center), size, color);
    }
    
    void ImageIOS::drawPolygon(std::vector<Point> &points, RGBA32Color color)
    {
        std::vector<Point> temp;
        
        for (Point p : points)
        {
            temp.push_back(pointToCGCoord(height, p));
        }
        
        CGDraw::DrawPolygon(_context, temp, color);
    }
    
    
    void ImageIOS::drawBezier(std::vector<Point> &points, RGBA32Color color)
    {
        std::vector<Point> temp;
        
        for (Point p : points)
        {
            temp.push_back(pointToCGCoord(height, p));
        }
        
        CGDraw::DrawBezier(_context, temp, color);
    }
}
