//
//  Image.h
//  primitiveApp
//
//  Created by Nikita Kunevich on 1/20/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef Image_h
#define Image_h

#include "Image.hpp"

namespace primitive {
    
    class ImageIOS : public Image {
        
        bool _managesMem;
        uint8_t* _buffer;
        CGContextRef _context;
    public:
        virtual void fillRect(Point p1, Point p2, RGBA32Color col) override;
        virtual RGBA32Color getPixelAt(uint32_t x, uint32_t y) override;
        virtual RGBA32Color getPixelAt(Point point) override;
        virtual void setPixelAt(Point point, RGBA32Color color) override;

        virtual ImagePtr copy(bool empty /* = false*/) override;
        
        virtual void drawTriangle(Point a, Point b, Point c, RGBA32Color color) override;
        virtual void drawRectangle(Point a, Point b, RGBA32Color color) override;
        virtual void drawEllipse(Point center, Point size, RGBA32Color color) override;
        virtual void drawPolygon(std::vector<Point> &points, RGBA32Color color) override;
        virtual void drawBezier(std::vector<Point> &points, RGBA32Color color) override;
        
        static Point& pointToCGCoord(uint32_t height, Point& point);

        
        CGImageRef getImage();
        ImageIOS(CGContextRef context);
        ImageIOS(CGContextRef context, uint8_t* buffer);
        ~ImageIOS();
    };
}
#endif /* Image_h */
