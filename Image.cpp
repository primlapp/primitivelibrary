//
//  Image.cpp
//  primitiveApp
//
//  Created by Nikita Kunevich on 1/20/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#include <CoreGraphics/CoreGraphics.h>

#include "Image.hpp"
#include "CGUtils.hpp"
#include "Shape.hpp"
#include <cmath>


namespace primitive
{
    RGBA32Color Image::getAverageColor()
    {
        uint64_t r= 0, g=0, b=0;
        for (int y = 0; y < height; y++) {
            for (int x= 0; x < width; x++) {
                RGBA32Color c = getPixelAt(x, y);
                r += c.r;
                g += c.g;
                b += c.b;
            }
        }
        uint32_t pixel_count = height * width;
        r /= pixel_count;
        g /= pixel_count;
        b /= pixel_count;
        return RGBA32Color{(uint8_t)r, (uint8_t)g, (uint8_t)b, 255};
    }
    
    double Image::differenceFull(ImagePtr other)
    {
        u_int64_t total = 0;
        
        for (uint32_t y = 0; y < height; y++)
            for (uint32_t x = 0; x < width; x++)
            {
                RGBA32Color aPixel = other->getPixelAt(x, y);
                RGBA32Color bPixel = this->getPixelAt(x, y);
                
                uint32_t dr, dg, db, da;
                dr = aPixel.r - bPixel.r;
                dg = aPixel.g - bPixel.g;
                db = aPixel.b - bPixel.b;
                da = aPixel.a - bPixel.a;
                
                total += dr*dr + dg*dg + db*db + da*da;
            }
        
        u_int64_t totalComponents = width * height * 4;
        return std::sqrt(total / (double)totalComponents) / 255;
    }
}
