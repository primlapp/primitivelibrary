//
//  Primitivizer.cpp
//  primitiveApp
//
//  Created by Nikita Kunevich on 1/21/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#include <algorithm>
#include <thread>
#include <vector>
#include <future>

#include "Primitivizer.hpp"
#include "TriangleShape.hpp"
#include "Worker.hpp"
#include "State.hpp"
#include "Utility.hpp"

#define onecore 1



namespace primitive
{    
    const int32_t default_alpha = 225;
    const int32_t default_repeat = 0;    
    
    PrimitivizerPtr Primitivizer::_instance;
    
    PrimitivizerPtr Primitivizer::getInstance()
    {
        if (!Primitivizer::_instance)
        {
            Primitivizer::_instance = PrimitivizerPtr(new Primitivizer);
        }
        return Primitivizer::_instance;
    }
    
    Primitivizer::Primitivizer() : _current(nullptr), _target(nullptr)
    {
        this->genParams = primitive::AutoPrecision;
    }
    
    
    void Primitivizer::setShapesMultipliers(float mult) {
        TriangleShape::setMultiplier(Shape::_size_multiplier * mult);
        RectangleShape::setMultiplier(Shape::_size_multiplier * mult);
        EllipseShape::setMultiplier(Shape::_size_multiplier * mult);
        BezierShape::setMultiplier(Shape::_size_multiplier * mult);
        PolygonShape::setMultiplier(Shape::_size_multiplier * mult);
    }
    
    void Primitivizer::Init(CGImageRef image_ref, uint32_t max_dimension)
    {
        size_t width = CGImageGetWidth(image_ref);
        size_t height = CGImageGetHeight(image_ref);
        size_t new_width;
        size_t new_height;
        size_t max_img_dimension = std::max(width, height);
        if (max_img_dimension > max_dimension)
        {
            //resize
            bool width_bigger = width > height;
            
            CGFloat aspect_ratio = (CGFloat)width / height;
            new_width = width_bigger ? max_dimension : aspect_ratio * max_dimension;
            new_height = width_bigger ? max_dimension / aspect_ratio : max_dimension;
            CGContextRef resized_image_context = ResizeCGImageAndGetContext(image_ref, new_width, new_height);
            _target = std::unique_ptr<ImageIOS>(new ImageIOS(resized_image_context));
        }
        else
        {
            CGContextRef context = CGContextFromImageRef(image_ref);
            _target = std::unique_ptr<ImageIOS>(new ImageIOS(context));
        }
        Shape::_size_multiplier = std::min(new_width, new_height) / (float)Shape::_orig_size;
        CGContextRef context = CGEmptyContextFromRect(CGRectMake(0, 0, _target->width, _target->height));
        _current = std::make_shared<ImageIOS>(context);
        
#ifdef real_out_size
        context = CGEmptyContextFromRect(CGRectMake(0, 0, width, height));
        _current_scaled = std::make_shared<ImageIOS>(context);
        
        _x_current_scaled_factor = (float)width / _target->width;
        _y_current_scaled_factor = (float)height / _target->height;
#endif

        _workers.clear();
        
#if onecore
        int core_count = 1;
#else
        int core_count = std::thread::hardware_concurrency();
#endif
        
        for (int i = 0; i < core_count; i++)
        {
            WorkerPtr worker = std::make_shared<Worker>(_target);
            _workers.push_back(worker);
        }
    }
    

    
    CGImageRef Primitivizer::getCurrentImage()
    {
#ifdef real_out_size
        return ((ImageIOS*)_current_scaled.get())->getImage();
#else
        return ((ImageIOS*)_current.get())->getImage();
#endif
    }

    
    void Primitivizer::add_shape(ShapePtr shape, int alpha)
    {
        //draw on image
        
        auto before = _current->copy();
        bool error = false;
        auto color = shape->computeColor(_target, _current, alpha, error);
        shape->draw(_current, color);
        
#ifdef real_out_size
        auto scaledShape = shape->scale(_x_current_scaled_factor, _y_current_scaled_factor);
        scaledShape->draw(_current_scaled, color);
#endif
        
        auto score = shape->differencePartial(_target, before, _current, _score);
        
        _score = score;
        _shapes.push_back(shape);
        _colors.push_back(color);
        _scores.push_back(score);
    }
    
    void Primitivizer::step(ShapeType shapeType)
    {
        step(shapeType, default_alpha, default_repeat, nullptr, -1);
    }
    
    const GenerationParameters Primitivizer::getGenerationParameters() {
        return this->genParams;
    }

    
    void Primitivizer::setGenerationParams(GenerationParameters params) {
        this->genParams = params;
    }
    
    void Primitivizer::step(ShapeType shapeType, Point base_point )
    {
#ifdef real_out_size
        auto scaled_base_point = Point::scale(base_point, 1.0f / _x_current_scaled_factor, 1.0f / _y_current_scaled_factor);
        step(shapeType, default_alpha, default_repeat, &scaled_base_point, 1);
#else
        step(shapeType, default_alpha, default_repeat, &base_point, 1);
#endif
    }
    
    void Primitivizer::step(ShapeType shapeType, int alpha, int repeat_count, Point * base_point_ptr, int shapes_to_add)
    {
        const int OUTER_ATTEMPT_COUNT = this->genParams.OUTER_ATTEMPT_COUNT;
        const int OPTIMIZE_ATTEMPT_COUNT = this->genParams.OPTIMIZE_ATTEMPT_COUNT;
        const int INNER_ATTEMPT_COUNT = this->genParams.INNER_ATTEMPT_COUNT;
        
        auto states = this->runWorkers(shapeType, alpha, INNER_ATTEMPT_COUNT, OPTIMIZE_ATTEMPT_COUNT, OUTER_ATTEMPT_COUNT, base_point_ptr);
        auto state_comp = [](const StatePtr &a, const StatePtr &b) { return a->energy() < b->energy(); };
        
        std::sort(states.begin(), states.end(), state_comp);
        
        double best_energy = states.front()->energy();
        
        //these settings are under consideration
        const double max_energy_diff = 0.001;
        int32_t states_to_add = shapes_to_add == -1 ? (int32_t)states.size() : shapes_to_add;
        
        for (int32_t count = 0; count < states_to_add; count++)
        {
            if (count != 0)
            {
                for (const auto &state : states)
                {
                    state->recomputeEnergy();
                }
                
                std::sort(states.begin(), states.end(), state_comp);
            }
            
            auto current_best_state = states.front();
            double current_best_energy = current_best_state->energy();
            
            double energy_diff = std::abs(best_energy - current_best_energy);
            
            if (energy_diff > max_energy_diff)
            {
                printf("Energy diff exceeded at %d state.\n", count);
                break;
            }
            
            this->add_shape(current_best_state->shape, current_best_state->alpha);
            states.erase(states.begin());
            
            int32_t worker_count = (int32_t)_workers.size();
            for (int i = 0; i < worker_count; i++)
            {
                auto worker = _workers[i];
                worker->init(_current, _score);
            }
        }
        
        
        // fix repeat
#ifdef ENABLE_REPEAT
        for (int i = 0; i < repeat_count; i++)
        {
            state->worker->init(_current, _score);
            double pre_energy = state->energy();
            state = std::dynamic_pointer_cast<State>(HillClimb(state, 10));
            
            double after_energy = state->energy();
            
            if (pre_energy == after_energy)
                break;
            
            add_shape(state->shape, state->alpha);
        }
#endif
    }
    
    std::vector<StatePtr> Primitivizer::runWorker(WorkerPtr worker, ShapeType shapeType, int alpha, int inner_count,
                                                  int optimize_max_age, int outer_count, Point * base_point_ptr)
    {
        return worker->bestHillClimbState(shapeType, alpha, outer_count, inner_count, optimize_max_age, base_point_ptr);
    }
    
    std::vector<StatePtr> Primitivizer::runWorkers(ShapeType shapeType, int alpha, int inner_count ,int optimize_max_age, int outer_count, Point * base_point_ptr)
    {
#if onecore
        _workers[0]->init(_current, _score);
        return Primitivizer::runWorker(_workers[0], shapeType, alpha, inner_count, optimize_max_age, outer_count, base_point_ptr);
#else
        int32_t worker_count = (int32_t)_workers.size();
        
        std::vector<std::future<std::vector<StatePtr>>> futures;
        for (int i = 0; i < worker_count; i++)
        {
            auto worker = _workers[i];
            worker->init(_current, _score);
            futures.push_back(std::async(Primitivizer::runWorker, worker, shapeType, alpha, inner_count, optimize_max_age, outer_count, base_point_ptr));
        }
        
        
        std::vector<StatePtr> results;
        for (int i = 0; i < worker_count; i++)
        {
            auto temp = futures[i].get();
            results.insert(results.begin(), temp.begin(), temp.end());
        }
        
        return results;
        
#endif
    }
    
    void Primitivizer::clear() {
        _shapes.clear();
        _colors.clear();
        _scores.clear();
    }
    
    bool Primitivizer::processImage(ShapeType shapeType, uint16_t shapeCount, on_step_block_t stepBlock)
    {
        _is_processing = true;
        
        prepareToProcess(ImageFillTypeAverage);
        for (int i = 0; i < shapeCount; i++)
        {
            if (!_stop_processing)
            {
                step(shapeType);
                
                if (stepBlock != NULL)
                {
                    stepBlock(((float)i) / shapeCount);
                }
            }
            else
            {
                _stop_processing = false;
//                _is_processing = false;
                if (_on_stop_block != NULL)
                {
                    _on_stop_block(false);
                    Block_release(_on_stop_block);
                    _on_stop_block = NULL;
                }
                                _is_processing = false;

                return false;
            }
        }
        
        
        _is_processing = false;
        
        return true;
    }
    
    void Primitivizer::prepareToProcess(ImageFillType fillType)
    {
        clear();

        if (fillType == ImageFillTypeAverage) {
            RGBA32Color bgColor = _target->getAverageColor();
            _current->fillRect(Point{0, 0}, Point{_current->width, _current->height}, bgColor);
        }
        else if (fillType == ImageFillTypeFinal) {
            _current = _target->copy();
        }
        else if (fillType == ImageFillTypeNo) {
            _current->fillRect(Point{0, 0}, Point{_current->width, _current->height}, {0, 0, 0, 0});
        }
        
        
#ifdef real_out_size
        RGBA32Color bgColor = _target->getAverageColor();
        _current_scaled->fillRect(Point{0, 0}, Point(_current_scaled->width, _current_scaled->height), bgColor);
#endif

        _score = _target->differenceFull(_current);
    }
    
    void Primitivizer::fillCurrent(CGImageRef image) {
#ifdef IOS
        _current = std::make_shared<ImageIOS>(CGContextFromImageRef(image));
#endif
    }
    
    void Primitivizer::stopProcessing()
    {
        _stop_processing = true;
    }
    
    void Primitivizer::stopProcessing(on_stop_block_t block)
    {
        if (!_is_processing)
        {
            block(true);
        }
        else
        {
            _on_stop_block = Block_copy(block);
            _stop_processing = true;
        }
    }
}


