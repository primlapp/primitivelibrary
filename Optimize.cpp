//
//  Optimize.cpp
//  primitiveApp
//
//  Created by Nikita Kunevich on 2/3/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#include "Optimize.hpp"
#include "State.hpp"
#include "Utility.hpp"
#include "Primitivizer.hpp"

#include <random>
#include <math.h>

//TODO: #2 - can improve performance if try Random-restart hill climbing
namespace primitive
{
    std::vector<AnnealablePtr> HillClimb(AnnealablePtr state, int maxAge)
    {
        int states_num = Primitivizer::getInstance()->getGenerationParameters().OPTIMIZE_RETURN_COUNT;
        std::vector<AnnealablePtr> best_states;
        
        AnnealablePtr current_state = state->copy();
        
        tryAddState(best_states, current_state, states_num);
        
        for (int age = 0; age < maxAge; age++)
        {
            AnnealablePtr undo = current_state->doMove();
            
            if (!tryAddState(best_states, current_state, states_num))
            {
                current_state->undoMove(undo);
            }
            else
            {
                age = -1;
            }
        }
        
        return best_states;
    }
    
    double PreAnneal(AnnealablePtr state, int iterations)
    {
        state = state->copy();
        auto previous = state->energy();
        double total;
        
        for (int i = 0; i < iterations; i++) {
            state->doMove();
            auto energy = state->energy();
            total += fabs(energy - previous);
            previous = energy;
        }
        return total / double(iterations);
    }
    
    AnnealablePtr Anneal(AnnealablePtr state, double max_temp, double min_temp, int steps)
    {
        auto factor = -log(max_temp / min_temp);
        state = state->copy();
        auto best_state(state->copy());
        auto best_energy = state->energy();
        auto previous_energy = best_energy;
        for (int step = 0; step < steps; step++) {
            auto pct = double(step) / double(steps - 1);
            auto temp = max_temp * exp(factor * pct);
            auto undo = state->doMove();
            auto energy = state->energy();
            auto change = energy - previous_energy;
            if (change > 0 && exp(-change/temp) < ((double)arc4random() / (RAND_MAX)))
            {
                state->undoMove(undo);
            }
            else
            {
                previous_energy = energy;
                if (energy < best_energy)
                {
                    best_energy = energy;
                    best_state = state->copy();
                }
            }
        }
        return AnnealablePtr(best_state);
    }
}

