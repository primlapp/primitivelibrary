//
//  ShapeTypes.h
//  primitiveApp
//
//  Created by Nikita Kunevich on 3/13/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef ShapeTypes_h
#define ShapeTypes_h


enum ShapeType
{
    Triangle            = 0,
    Rectangle           = 1,
    Ellipse             = 2,
    Polygon             = 3,
    Circle              = 4,
    RotatedRectangle    = 5,
    Bezier              = 6,
    Combo               = 7
};

#endif /* ShapeTypes_h */
