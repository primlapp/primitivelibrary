//
//  BezierShape.cpp
//  primitiveApp
//
//  Created by Anton Protko on 2/13/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#include "BezierShape.hpp"
#include "Worker.hpp"
#include "Utility.hpp"

#include <cstdlib>

namespace primitive
{
    const int base_size = 40;
    const int base_half = base_size / 2;
    const int rand_size = 16;
    
    float BezierShape::_mult = 1.f;
    int BezierShape::_mult_base = base_size;
    int BezierShape::_mult_half = base_half;
    int BezierShape::_mult_rand = rand_size;
    
    BezierShape::BezierShape(WorkerPtr worker, std::vector<Point> &points, bool mutate_base_point)
        : _points(points)
    {
        _mutate_base_point = mutate_base_point;
        _worker = worker;
    }
    
    void BezierShape::setMultiplier(float mult) {
        _mult = Shape::_size_multiplier * mult;
        _mult_base = roundf(base_size * _mult);
        _mult_half = roundf(base_half * _mult);
        _mult_rand = roundf(rand_size * _mult);
    }
    
    BezierShapePtr BezierShape::randomBezier(WorkerPtr worker, uint32_t max_x, uint32_t max_y)
    {
        return bezierFromPoint(worker, max_x, max_y, {arc4random() % max_x, arc4random() % max_y});
    }
    
    
    BezierShapePtr BezierShape::bezierFromPoint(WorkerPtr worker, uint32_t max_x, uint32_t max_y, Point point, bool mutate_base_point)
    {
        std::vector<Point> points;
        
        const int32_t points_num = 4;
        
        points.push_back(point);
        
        for (int32_t count = 1; count < points_num; count++)
        {
            Point p(points[0].x + arc4random() % _mult_base - _mult_half,
                    points[0].y + arc4random() % _mult_base - _mult_half);
            
            points.push_back(p);
        }
        
        BezierShapePtr bezier = std::make_shared<BezierShape>(worker, points, mutate_base_point);
        bezier->mutate(max_x, max_y);
        
        return bezier;
    }
    
    
    void BezierShape::draw(ImagePtr& target, RGBA32Color& color)
    {
        target->drawBezier(_points, color);
    }
    
    
    ShapePtr BezierShape::copy()
    {
        return std::make_shared<BezierShape>(_worker, _points, _mutate_base_point);
    }
    
    
    void BezierShape::mutate(uint32_t max_x, uint32_t max_y)
    {
        int32_t a = _mutate_base_point ? _points.size() : _points.size() - 1;
        int32_t b = _mutate_base_point ? 0 : 1;
        
        int32_t to_change = (arc4random() % a) + b;
        
        _points[to_change].x += _worker->normRand() * _mult_rand;
        _points[to_change].y += _worker->normRand() * _mult_rand;
    }
    
    
    RGBA32Color BezierShape::computeColor(ImagePtr& target, ImagePtr& current, int alpha, bool& error)
    {
        RGBA32Color color;
        
        uint64_t rSum, gSum, bSum;
        uint64_t count = 0;
        
        rSum = 0;
        gSum = 0;
        bSum = 0;
        
        std::vector<Point> points = bezierPath(_points[0], _points[1], _points[2], _points[3], target->width, target->height);
        for (const Point &p : points)
        {
            RGBA32Color targetPixel = target->getPixelAt(p);
            RGBA32Color currentPixel = current->getPixelAt(p);
            
            rSum += (targetPixel.r - currentPixel.r) * alpha + currentPixel.r * 0x101;
            gSum += (targetPixel.g - currentPixel.g) * alpha + currentPixel.g * 0x101;
            bSum += (targetPixel.b - currentPixel.b) * alpha + currentPixel.b * 0x101;
            
            count++;
        }
        
        if (!count)
        {
            error = true;
            return RGBA32Color();
        }
        
        
        color.r = clamp<uint64_t>((rSum / count) >> 8, 0, 255);
        color.b = clamp<uint64_t>((bSum / count) >> 8, 0, 255);
        color.g = clamp<uint64_t>((gSum / count) >> 8, 0, 255);
        color.a = alpha;
        
        return color;

    }
    
    double BezierShape::differencePartial(ImagePtr& target, ImagePtr& before, ImagePtr& after, double score)
    {
        int64_t total = pow(score * 255, 2) * double(target->width * target->height * 4);
        
        std::vector<Point> points = bezierPath(_points[0], _points[1], _points[2], _points[3], target->width, target->height);
        
        for (const Point &point : points)
        {
            RGBA32Color targetPixel = target->getPixelAt(point);
            RGBA32Color beforePixel = before->getPixelAt(point);
            RGBA32Color afterPixel = after->getPixelAt(point);
            
            int32_t dr1 = targetPixel.r - beforePixel.r;
            int32_t dg1 = targetPixel.g - beforePixel.g;
            int32_t db1 = targetPixel.b - beforePixel.b;
            int32_t da1 = targetPixel.a - beforePixel.a;
            
            int32_t dr2 = targetPixel.r - afterPixel.r;
            int32_t dg2 = targetPixel.g - afterPixel.g;
            int32_t db2 = targetPixel.b - afterPixel.b;
            int32_t da2 = targetPixel.a - afterPixel.a;
            
            total -= dr1*dr1 + dg1*dg1 + db1*db1 + da1*da1;
            total += dr2*dr2 + dg2*dg2 + db2*db2 + da2*da2;
        }
        
        return std::sqrt(total / ((double)target->width * target->height * 4)) / 255.f;
    }
    
    
    void BezierShape::copyPixels(ImagePtr& target, ImagePtr& current)
    {
        std::vector<Point> points = bezierPath(_points[0], _points[1], _points[2], _points[3], target->width, target->height);
        
        for (const Point &point : points)
        {
            target->setPixelAt(point, current->getPixelAt(point));
        }
    }
    
    
    void BezierShape::getBoundingRect(Point &min, Point &max)
    {
        
    }
    
    
    ShapePtr BezierShape::scale(float x_factor, float y_factor)
    {
        std::vector<Point> scaled_points;
        
        for (auto &point : _points)
        {
            scaled_points.push_back(Point::scale(point, x_factor, y_factor));
        }
        
        return std::make_shared<PolygonShape>(_worker, scaled_points, _mutate_base_point);
    }
}
