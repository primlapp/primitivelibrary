//
//  EllipseShape.hpp
//  primitiveApp
//
//  Created by Anton Protko on 2/9/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef EllipseShape_hpp
#define EllipseShape_hpp

#include "Shape.hpp"

namespace primitive
{
    class EllipseShape;
    typedef std::shared_ptr<EllipseShape> EllipseShapePtr;
    
    class EllipseShape : public Shape
    {
    public:
        EllipseShape(WorkerPtr, Point center, Point size, bool circle, bool mutate_base_point);
        
        static void setMultiplier(float mult);
        
        static EllipseShapePtr randomEllipse(WorkerPtr worker, uint32_t max_x, uint32_t max_y);
        static EllipseShapePtr ellipseFromPoint(WorkerPtr worker, uint32_t max_x, uint32_t max_y, Point point, bool mutate_base_point = false);
        
        static EllipseShapePtr randomCircle(WorkerPtr worker, uint32_t max_x, uint32_t max_y);
        static EllipseShapePtr circleFromPoint(WorkerPtr worker, uint32_t max_x, uint32_t max_y, Point point, bool mutate_base_point = false);

        virtual void draw(ImagePtr& target, RGBA32Color& color) override;
        virtual ShapePtr copy() override;
        virtual void getBoundingRect(Point &min, Point &max) override;
        
        virtual bool pointInShape(Point &point) override;        
        virtual void mutate(uint32_t max_x, uint32_t max_y) override;
        
        virtual ShapePtr scale(float x_factor, float y_factor) override;

        
    private:
        static float _mult;
        static int _mult_base;
        static int _mult_half;

        Point _center, _size;
        
        bool _circle;
    };
}

#endif /* EllipseShape_hpp */
