//
//  TriangleShape.hpp
//  primitiveApp
//
//  Created by maveric on 1/21/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef TriangleShape_hpp
#define TriangleShape_hpp

#include "Shape.hpp"

namespace primitive
{
    class TriangleShape;
    typedef std::shared_ptr<TriangleShape> TriangleShapePtr;
    
    
    class TriangleShape : public Shape
    {
    public:
        TriangleShape(WorkerPtr, Point _a, Point _b, Point _c, bool);
        
        static void setMultiplier(float mult);
        
        static TriangleShapePtr randomTriangle(WorkerPtr worker, uint32_t max_x, uint32_t max_y);
        static TriangleShapePtr triangleFromPoint(WorkerPtr worker, uint32_t max_x, uint32_t max_y, Point base_point, bool mutate_first = false);
        
        static bool point_in_triangle_barycentric(Point s, Point a, Point b, Point c);
        
        virtual void getBoundingRect(Point &min, Point &max) override;
        virtual bool pointInShape(Point &point) override;
        virtual void draw(ImagePtr& target, RGBA32Color& color) override;
        virtual ShapePtr copy() override;

        virtual void mutate(uint32_t max_x, uint32_t max_y) override;
        
        virtual ShapePtr scale(float x_factor, float y_actor) override;
        
        bool valid();
        

    private:
        static float _mult;
        static int _mult_base;
        static int _mult_half;
        Point _p1, _p2, _p3;
    };
    
}

#endif /* TriangleShape_hpp */
