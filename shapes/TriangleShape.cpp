//
//  TriangleShape.cpp
//  primitiveApp
//
//  Created by maveric on 1/21/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#include "TriangleShape.hpp"
#include "Utility.hpp"
#include "Worker.hpp"

#include <cstdlib>
#include <iostream>
#include <cmath>
#include <random>
#include <assert.h>

namespace primitive
{
    const int base_size = 32;
    const int base_half = base_size / 2;
    
    float TriangleShape::_mult = 1.f;
    int TriangleShape::_mult_base = base_size;
    int TriangleShape::_mult_half = base_half;
    
    
    TriangleShape::TriangleShape(WorkerPtr worker, Point a, Point b, Point c, bool mutate_base_point)
    {
        _worker = worker;
        _p1 = a;
        _p2 = b;
        _p3 = c;
        _mutate_base_point = mutate_base_point;

    }
    
    
    void TriangleShape::setMultiplier(float mult) {
        _mult = Shape::_size_multiplier * mult;
        _mult_base = roundf(base_size * _mult);
        _mult_half = roundf(base_half * _mult);
    }
    
    TriangleShapePtr TriangleShape::randomTriangle(WorkerPtr worker, uint32_t max_x, uint32_t max_y)
    {
        return triangleFromPoint(worker, max_x, max_y, { arc4random() % max_x, arc4random() % max_y }, true);
    }
    
    
    TriangleShapePtr TriangleShape::triangleFromPoint(WorkerPtr worker, uint32_t max_x, uint32_t max_y, Point base_point, bool mutate_base)
    {
        uint32_t x1 = base_point.x;
        uint32_t y1 = base_point.y;
        
        uint32_t x2 = x1 + arc4random() % (_mult_base - 1) - _mult_half - 1;
        uint32_t y2 = y1 + arc4random() % (_mult_base - 1) - _mult_half - 1;
        uint32_t y3 = y1 + arc4random() % (_mult_base - 1) - _mult_half - 1;
        uint32_t x3 = x1 + arc4random() % (_mult_base - 1) - _mult_half - 1;
        
        Point x{x1, y1};
        Point y{x2, y2};
        Point z{x3, y3};
        
        TriangleShapePtr triangle = std::make_shared<TriangleShape>(worker, x, y, z, mutate_base);
        triangle->mutate(max_x, max_y);
        
        return triangle;
    }
    
    
    void TriangleShape::draw(ImagePtr& target, RGBA32Color& color)
    {
        target->drawTriangle(_p1, _p2, _p3, color);
    }
    

    
    void TriangleShape::mutate(uint32_t max_x, uint32_t max_y)
    {
        for (;;)
        {
            int point_to_mutate = _mutate_base_point ? arc4random() % 3 : (arc4random() % 2) + 1;
            switch (point_to_mutate)
            {
                case 0:
                    _p1.x = clamp<int32_t>(_p1.x + int32_t(_worker->normRand() * _mult_half), 0, max_x-1);
                    _p1.y = clamp<int32_t>(_p1.y + int32_t(_worker->normRand() * _mult_half), 0, max_y-1);
                    break;
                    
                case 1:
                    _p2.x = clamp<int32_t>(_p1.x + int32_t(_worker->normRand()  * _mult_half), 0, max_x-1);
                    _p2.y = clamp<int32_t>(_p1.y + int32_t(_worker->normRand() * _mult_half), 0, max_y-1);
                    break;
                    
                case 2:
                    _p3.x = clamp<int32_t>(_p1.x + int32_t(_worker->normRand() * _mult_half), 0, max_x-1);
                    _p3.y = clamp<int32_t>(_p1.y + int32_t(_worker->normRand() * _mult_half), 0, max_y-1);
                    break;
                
                default:
                    break;
            }
        
            _p1.x = clamp<int32_t>(_p1.x, 0, max_x);
            _p1.y = clamp<int32_t>(_p1.y, 0, max_y);
            _p2.x = clamp<int32_t>(_p2.x, 0, max_x);
            _p2.y = clamp<int32_t>(_p2.y, 0, max_y);
            _p3.x = clamp<int32_t>(_p3.x, 0, max_x);
            _p3.y = clamp<int32_t>(_p3.y, 0, max_y);
            
            if (this->valid())
                break;
        }
    }
    
    bool TriangleShape::valid()
    {
        const int min_degrees = 30;
        double a1, a2, a3;
        {
            double x1 = _p2.x - _p1.x;
            double y1 = _p2.y - _p1.y;
            double x2 = _p3.x - _p1.x;
            double y2 = _p3.y - _p1.y;
            
            double d1 = sqrt(x1*x1 + y1*y1);
            double d2 = sqrt(x2*x2 + y2*y2);
            
            x1 /= d1;
            y1 /= d1;
            
            x2 /= d2;
            y2 /= d2;
            
            a1 = acos(x1*x2 + y1*y2) * 180 / M_PI;
        }
        
        {
            double x1 = _p1.x - _p2.x;
            double y1 = _p1.y - _p2.y;
            double x2 = _p3.x - _p2.x;
            double y2 = _p3.y - _p2.y;
            
            double d1 = sqrt(x1*x1 + y1*y1);
            double d2 = sqrt(x2*x2 + y2*y2);
            
            x1 /= d1;
            y1 /= d1;
            
            x2 /= d2;
            y2 /= d2;
            
            a2 = acos(x1*x2 + y1*y2) * 180 / M_PI;
        }
        a3 = 180 - a1 - a2;
        
        return a1 > min_degrees && a2 > min_degrees && a3 > min_degrees;
    }

    
    ShapePtr TriangleShape::copy()
    {
        return std::make_shared<TriangleShape>(_worker, this->_p1, this->_p2, this->_p3, this->_mutate_base_point);
    }

    void TriangleShape::getBoundingRect(Point &min, Point &max)
    {
        uint32_t minX = std::min({_p1.x, _p2.x, _p3.x});
        uint32_t minY = std::min({_p1.y, _p2.y, _p3.y});
        uint32_t maxX = std::max({_p1.x, _p2.x, _p3.x});
        uint32_t maxY = std::max({_p1.y, _p2.y, _p3.y});
        
        min.x = minX;
        min.y = minY;
        
        max.x = maxX;
        max.y = maxY;
    }
    
    bool TriangleShape::pointInShape(Point &point)
    {
        auto denominator = ((_p2.y - _p3.y) * (_p1.x - _p3.x) + (_p3.x - _p2.x) * (_p1.y - _p3.y));
        auto i = ((_p2.y - _p3.y) * (point.x - _p3.x) + (_p3.x - _p2.x) * (point.y - _p3.y)) / (float)denominator;
        auto j = ((_p3.y - _p1.y) * (point.x - _p3.x) + (_p1.x - _p3.x) * (point.y - _p3.y)) / (float)denominator;
        auto k = 1 - i - j;
        
        return 0 <= i && i <= 1 && 0 <= j && j <= 1 && 0 <= k && k <= 1;
    }
    
    ShapePtr TriangleShape::scale(float x_factor, float y_factor)
    {
        return std::make_shared<TriangleShape>(_worker,
                                               Point::scale(_p1, x_factor, y_factor),
                                               Point::scale(_p2, x_factor, y_factor),
                                               Point::scale(_p3, x_factor, y_factor),
                                               this->_mutate_base_point);
    }
}
