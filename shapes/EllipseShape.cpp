//
//  EllipseShape.cpp
//  primitiveApp
//
//  Created by Anton Protko on 2/9/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#include "EllipseShape.hpp"
#include "Utility.hpp"
#include "Worker.hpp"
#include "Primitivizer.hpp"
#include <cmath>
#include <cstdlib>

namespace primitive
{
    const int base_size = 32;
    const int base_half = base_size / 2;
    
    float EllipseShape::_mult = 1.f;
    int EllipseShape::_mult_base = base_size;
    int EllipseShape::_mult_half = base_half;
    
    
    EllipseShape::EllipseShape(WorkerPtr worker, Point center, Point size, bool circle, bool mutate_base_point)
    {
        _center = center;
        _size = size;
        _circle = circle;
        _mutate_base_point = mutate_base_point;
        
        _worker = worker;
    }
    
    void EllipseShape::setMultiplier(float mult) {
        _mult = Shape::_size_multiplier * mult;
        _mult_base = roundf(base_size * _mult);
        _mult_half = roundf(base_half * _mult);
    }
    
    
    void EllipseShape::draw(ImagePtr& target, RGBA32Color& color)
    {
//        RGBA32Color col = target->getPixelAt(_center);
        target->drawEllipse(_center, _size, color);
//        RGBA32Color colw = target->getPixelAt(_center);
    }
    
    EllipseShapePtr EllipseShape::randomEllipse(WorkerPtr worker, uint32_t max_x, uint32_t max_y)
    {
        return ellipseFromPoint(worker, max_x, max_y, {arc4random() % max_x, arc4random() % max_y}, true);
    }
    
    
    EllipseShapePtr EllipseShape::randomCircle(WorkerPtr worker, uint32_t max_x, uint32_t max_y)
    {
        return circleFromPoint(worker, max_x, max_y, {arc4random() % max_x, arc4random() % max_y}, true);
    }
    
    
    EllipseShapePtr EllipseShape::ellipseFromPoint(WorkerPtr worker, uint32_t max_x, uint32_t max_y, Point point, bool mutate_base_point)
    {
        uint32_t x1 = point.x;
        uint32_t y1 = point.y;
        
        uint32_t x2 = arc4random() % (_mult_base - 1) + 5;
        uint32_t y2 = arc4random() % (_mult_base - 1) + 5;
        
        Point center {x1, y1};
        Point size {x2, y2};
        
        EllipseShapePtr rect = std::make_shared<EllipseShape>(worker, center, size, false, mutate_base_point);
        rect->mutate(max_x, max_y);
        
        return rect;
    }
    
    
    EllipseShapePtr EllipseShape::circleFromPoint(WorkerPtr worker, uint32_t max_x, uint32_t max_y, Point point, bool mutate_base_point)
    {
        uint32_t x = point.x;
        uint32_t y = point.y;
        
        uint32_t r = arc4random() % (_mult_base - 1) + 5;
        
        Point center {x, y};
        Point size {r, r};
        
        EllipseShapePtr rect = std::make_shared<EllipseShape>(worker, center, size, true, mutate_base_point);
        rect->mutate(max_x, max_y);
        
        return rect;
    }
    
    
    ShapePtr EllipseShape::copy()
    {
        return std::make_shared<EllipseShape>(_worker, _center, _size, _circle, _mutate_base_point);
    }
    
    
    void EllipseShape::mutate(uint32_t max_x, uint32_t max_y)
    {
        int32_t a = _circle ? 2 : 3;
        int32_t b = _mutate_base_point ? 0 : 1;
        
        if (!_mutate_base_point)
            a--;
        
        switch ((arc4random() % a) + b)
        {
            case 0:
                _center.x = _center.x + int32_t(_worker->normRand() * _mult_half);
                _center.y = _center.y + int32_t(_worker->normRand() * _mult_half);
                break;
                
            case 1:
                _size.x = _size.x + int32_t(_worker->normRand()  * _mult_half);
                if (_circle)
                {
                    _size.y = _size.x;
                }
                
                break;
                
            case 2:
                _size.y = _size.y + int32_t(_worker->normRand() * _mult_half);
                break;
                
            default:
                break;
        }
        
        _center.clamp(0, max_x - 1, 0, max_y - 1);
        _size.clamp(2, max_x, 2, max_y);
    }
    
    
    void EllipseShape::getBoundingRect(Point &min, Point &max)
    {
        uint32_t minX = clamp<int32_t>(_center.x - _size.x / 2, 0, _worker->width - 1);
        uint32_t minY = clamp<int32_t>(_center.y - _size.y / 2, 0, _worker->height - 1);
        uint32_t maxX = clamp<int32_t>(_center.x + _size.x / 2, 0, _worker->width - 1);
        uint32_t maxY = clamp<int32_t>(_center.y + _size.y / 2, 0, _worker->height - 1);
        
        min.x = minX;
        min.y = minY;
        
        max.x = maxX;
        max.y = maxY;
    }
    
    
    bool EllipseShape::pointInShape(Point &point)
    {
#pragma mark TODO check which is faster
        //return true;
        
        int32_t dx = point.x - _center.x;
        int32_t dy = point.y - _center.y;
        
        float a = _size.x * 0.5f;
        float b = _size.y * 0.5f;
        
        float first = dx / a;
        float second = dy / b;
        
        return (first * first + second * second) <= 1.0f;
    }
    
    ShapePtr EllipseShape::scale(float x_factor, float y_factor)
    {
        return std::make_shared<EllipseShape>(_worker,
                                              Point::scale(_center, x_factor, y_factor),
                                              Point::scale(_size, x_factor, y_factor),
                                              _circle, _mutate_base_point);
    }
}
