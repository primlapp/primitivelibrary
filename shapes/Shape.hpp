//
//  Shape.hpp
//  primitiveApp
//
//  Created by maveric on 1/21/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef Shape_hpp
#define Shape_hpp

#include <vector>
#include <memory>

#include "Color.hpp"
#include "Image.hpp"


namespace primitive
{
    class Shape;
    typedef std::shared_ptr<Shape> ShapePtr;
    
    class Worker;
    typedef std::shared_ptr<Worker> WorkerPtr;
   
    class Shape
    {
    public:
        ShapeType type;
        static double _size_multiplier;
        static int _orig_size;
        virtual ShapePtr copy() = 0;
        
        virtual void mutate(uint32_t max_x, uint32_t max_y) = 0;
        virtual void draw(ImagePtr& image, RGBA32Color& color) = 0;
        virtual void copyPixels(ImagePtr& target, ImagePtr& current);
        
        virtual void getBoundingRect(Point &min, Point &max) = 0;
        virtual bool pointInShape(Point &point);

        virtual RGBA32Color computeColor(ImagePtr& target, ImagePtr& current, int alpha, bool& error);
        virtual double differencePartial(ImagePtr& target, ImagePtr& before, ImagePtr& after, double score);
        
        virtual ShapePtr scale(float x_factor, float y_factor) = 0;
        
    protected:
        WorkerPtr _worker;
        bool _mutate_base_point;
    };

}

#endif /* Shape_hpp */
