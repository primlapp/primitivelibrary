//
//  PolygonShape.hpp
//  primitiveApp
//
//  Created by Anton Protko on 2/10/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef PolygonShape_hpp
#define PolygonShape_hpp

#include "Shape.hpp"

namespace primitive
{
    class PolygonShape;
    typedef std::shared_ptr<PolygonShape> PolygonShapePtr;
    
    class PolygonShape : public Shape
    {
    public:
        PolygonShape(WorkerPtr worker, std::vector<Point> &points, bool mutate_base_point);
        static void setMultiplier(float mult);

        static PolygonShapePtr randomPolygon(WorkerPtr worker, uint32_t max_x, uint32_t max_y);
        static PolygonShapePtr polygonFromPoint(WorkerPtr worker, uint32_t max_x, uint32_t max_y, Point point, bool mutate_base_point = false);

        virtual void draw(ImagePtr& target, RGBA32Color& color) override;
        virtual ShapePtr copy() override;
        virtual void getBoundingRect(Point &min, Point &max) override;
        
        virtual void mutate(uint32_t max_x, uint32_t max_y) override;
        
        virtual ShapePtr scale(float x_factor, float y_factor) override;
        
    private:
        static float _mult;
        static int _mult_base;
        static int _mult_half;
        static int _mult_rand;
        std::vector<Point> _points;
    };
}

#endif /* PolygonShape_hpp */
