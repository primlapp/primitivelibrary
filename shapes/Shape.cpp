//
//  Shape.cpp
//  primitiveApp
//
//  Created by maveric on 1/21/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#include "Shape.hpp"
#include "Utility.hpp"

#include <cmath>
#include <iostream>

#ifndef TARGET_IPHONE_SIMULATOR
#include <arm_neon.h>
#endif

namespace primitive
{
    double Shape::_size_multiplier = 1.0;
    int Shape::_orig_size = 256;
    double Shape::differencePartial(ImagePtr& target, ImagePtr& before, ImagePtr& after, double score)
    {
        int64_t total = pow(score * 255, 2) * double(target->width * target->height * 4);
        
        Point min, max;
        getBoundingRect(min, max);
        
        uint32_t minX = min.x;
        uint32_t minY = min.y;
        uint32_t maxX = max.x;
        uint32_t maxY = max.y;
        
        
        for (uint32_t x = minX; x < maxX; x++)
            for (uint32_t y = minY + ((x & 1) ? 1 : 0); y < maxY; y += 3)
            {
                Point point{x, y};
                if (pointInShape(point))
                {
                    RGBA32Color targetPixel = target->getPixelAt(point);
                    RGBA32Color beforePixel = before->getPixelAt(point);
                    RGBA32Color afterPixel = after->getPixelAt(point);
                    
#ifndef TARGET_IPHONE_SIMULATOR
                    
                    int16x8_t a, b;
                    a = vsetq_lane_s16(targetPixel.r, a, 0);
                    a = vsetq_lane_s16(targetPixel.g, a, 1);
                    a = vsetq_lane_s16(targetPixel.b, a, 2);
                    a = vsetq_lane_s16(targetPixel.a, a, 3);
                    
                    a = vsetq_lane_s16(targetPixel.r, a, 4);
                    a = vsetq_lane_s16(targetPixel.g, a, 5);
                    a = vsetq_lane_s16(targetPixel.b, a, 6);
                    a = vsetq_lane_s16(targetPixel.a, a, 7);
                    
                    b = vsetq_lane_s16(afterPixel.r, b, 0);
                    b = vsetq_lane_s16(afterPixel.g, b, 1);
                    b = vsetq_lane_s16(afterPixel.b, b, 2);
                    b = vsetq_lane_s16(afterPixel.a, b, 3);
                    
                    b = vsetq_lane_s16(beforePixel.r, b, 4);
                    b = vsetq_lane_s16(beforePixel.g, b, 5);
                    b = vsetq_lane_s16(beforePixel.b, b, 6);
                    b = vsetq_lane_s16(beforePixel.a, b, 7);
                    
                    int16x8_t result = vsubq_s16(a, b);
                    
                    result = vmulq_s16(result, result);
                    
                    int32x4_t temp = vpaddlq_s16(result);
                    int64x2_t sum = vpaddlq_u32(temp);
                    
                    total += vgetq_lane_s64(sum, 0);
                    total -= vgetq_lane_s64(sum, 1);
#else
                    
                    int32_t dr1 = targetPixel.r - beforePixel.r;
                    int32_t dg1 = targetPixel.g - beforePixel.g;
                    int32_t db1 = targetPixel.b - beforePixel.b;
                    int32_t da1 = targetPixel.a - beforePixel.a;
                    
                    int32_t dr2 = targetPixel.r - afterPixel.r;
                    int32_t dg2 = targetPixel.g - afterPixel.g;
                    int32_t db2 = targetPixel.b - afterPixel.b;
                    int32_t da2 = targetPixel.a - afterPixel.a;
                    
                    total -= dr1*dr1 + dg1*dg1 + db1*db1 + da1*da1;
                    total += dr2*dr2 + dg2*dg2 + db2*db2 + da2*da2;
#endif

                }
            }
        
        return std::sqrt(total / ((double)target->width * target->height * 4)) / 255.f;
    }
    
    RGBA32Color Shape::computeColor(ImagePtr& target, ImagePtr& current, int alpha, bool& error)
    {
        RGBA32Color color;
        
        uint64_t rSum, gSum, bSum;
        uint64_t count = 0;
        
        rSum = 0;
        gSum = 0;
        bSum = 0;
        
        Point min, max;
        getBoundingRect(min, max);        
        
        uint32_t minX = min.x;
        uint32_t minY = min.y;
        uint32_t maxX = max.x;
        uint32_t maxY = max.y;
        
        
        for (uint32_t x = minX; x < maxX; x++)
            for (uint32_t y = minY + ((x & 1) ? 1 : 0); y < maxY; y += 3)
            {
                Point p = {x, y};
                if (pointInShape(p))
                {
                    RGBA32Color targetPixel = target->getPixelAt(p);
                    RGBA32Color currentPixel = current->getPixelAt(p);

#ifndef TARGET_IPHONE_SIMULATOR
                    int16x4_t target_v, current_v, magic_v, alpha_v;
                    target_v = vset_lane_s16(targetPixel.r, target_v, 0);
                    target_v = vset_lane_s16(targetPixel.g, target_v, 1);
                    target_v = vset_lane_s16(targetPixel.b, target_v, 2);
                    
                    current_v = vset_lane_s16(currentPixel.r, current_v, 0);
                    current_v = vset_lane_s16(currentPixel.g, current_v, 1);
                    current_v = vset_lane_s16(currentPixel.b, current_v, 2);
                    
                    magic_v = vdup_n_s16(0x101);
                    alpha_v = vdup_n_s16(int16_t(alpha));
                    
                    int16x4_t sub_v = vsub_s16(target_v, current_v);
                    sub_v = vmul_s16(sub_v, alpha_v);
                    int16x4_t result = vmla_s16(sub_v, current_v, magic_v);
                    
                    rSum += vget_lane_s16(result, 0);
                    gSum += vget_lane_s16(result, 1);
                    bSum += vget_lane_s16(result, 2);
#else
                    rSum += (targetPixel.r - currentPixel.r) * alpha + currentPixel.r * 0x101;
                    gSum += (targetPixel.g - currentPixel.g) * alpha + currentPixel.g * 0x101;
                    bSum += (targetPixel.b - currentPixel.b) * alpha + currentPixel.b * 0x101;
                    
#endif
                    count++;
                }
            }
        
        if (!count)
        {
            error = true;
            return RGBA32Color();
        }
        
        
        color.r = clamp<uint64_t>((rSum / count) >> 8, 0, 255);
        color.b = clamp<uint64_t>((bSum / count) >> 8, 0, 255);
        color.g = clamp<uint64_t>((gSum / count) >> 8, 0, 255);
        color.a = alpha;
        
        return color;
    }
    
    void Shape::copyPixels(ImagePtr& target, ImagePtr& current)
    {
        Point min, max;
        getBoundingRect(min, max);
        
        uint32_t minX = min.x;
        uint32_t minY = min.y;
        uint32_t maxX = max.x;
        uint32_t maxY = max.y;
        
        for (uint32_t x = minX; x < maxX; x++)
            for (uint32_t y = minY; y < maxY; y++)
            {
                Point point{x, y};
                if (pointInShape(point))
                {
                    target->setPixelAt(point, current->getPixelAt(point));
                }
            }
    }
    
    
    bool Shape::pointInShape(Point &point)
    {
        return true;
    }
}
