//
//  RectangleShape.hpp
//  primitiveApp
//
//  Created by Anton Protko on 2/8/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef RectangleShape_hpp
#define RectangleShape_hpp

#include "Shape.hpp"

namespace primitive
{
    class RectangleShape;
    typedef std::shared_ptr<RectangleShape> RectangleShapePtr;
    
    class RectangleShape : public Shape
    {
    public:
        static void setMultiplier(float mult);

        RectangleShape(WorkerPtr, Point a, Point b, bool mutate_base_point);
        RectangleShape(WorkerPtr worker, Point a, Point b, uint32_t angle, bool mutate_base_point);
        
        static RectangleShapePtr randomRectangle(WorkerPtr worker, uint32_t max_x, uint32_t max_y, bool rotate);
        static RectangleShapePtr rectangleFromPoint(WorkerPtr worker, uint32_t max_x, uint32_t max_y, bool rotate, Point base_point, bool mutate_base_point = false);
        
        virtual void draw(ImagePtr& target, RGBA32Color& color) override;
        virtual ShapePtr copy() override;
        virtual void getBoundingRect(Point &min, Point &max) override;
        virtual void mutate(uint32_t max_x, uint32_t max_y) override;
        virtual ShapePtr scale(float xFactor, float yFactor) override;
        
    private:
        static float _mult;
        static int _mult_base;
        static int _mult_half;
        
        Point _p1, _p2;
        
        int32_t _angle;
        bool _rotate;
    };
}

#endif /* RectangleShape_hpp */
