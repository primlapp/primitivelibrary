//
//  RectangleShape.cpp
//  primitiveApp
//
//  Created by Anton Protko on 2/8/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#include "RectangleShape.hpp"
#include "Utility.hpp"
#include "Worker.hpp"

#include <cmath>
#include <cstdlib>

namespace primitive
{
    const int base_size = 32;
    const int base_half = base_size / 2;
    
    float RectangleShape::_mult = 1.f;
    int RectangleShape::_mult_base = base_size;
    int RectangleShape::_mult_half = base_half;
    
    RectangleShape::RectangleShape(WorkerPtr worker, Point a, Point b, bool mutate_base_point)
    {
        _p1 = a;
        _p2 = b;
        
        _worker = worker;
        _rotate = false;
        
        _mutate_base_point = mutate_base_point;
    }    
    
    RectangleShape::RectangleShape(WorkerPtr worker, Point a, Point b, uint32_t angle, bool mutate_base_point)
        : RectangleShape(worker, a, b, mutate_base_point)
    {
        _rotate = true;
        _angle = angle;
    }
    
    void RectangleShape::setMultiplier(float mult) {
        _mult = Shape::_size_multiplier * mult;
        _mult_base = roundf(base_size * _mult);
        _mult_half = roundf(base_half * _mult);
    }
    
    void RectangleShape::draw(ImagePtr& target, RGBA32Color& color)
    {
        if (_rotate)
        {
            Point a(_p1.x, _p2.y);
            Point b(_p2);
            Point c(_p2.x, _p1.y);
            
            a.rotate(_p1, _angle);
            b.rotate(_p1, _angle);
            c.rotate(_p1, _angle);
            
            std::vector<Point> points;
            points.push_back(_p1);
            points.push_back(a);
            points.push_back(b);
            points.push_back(c);
            
            target->drawPolygon(points, color);
        }
        else
        {
            target->drawRectangle(_p1, _p2, color);
        }
    }
    
    RectangleShapePtr RectangleShape::randomRectangle(WorkerPtr worker, uint32_t max_x, uint32_t max_y, bool rotate)
    {
        return rectangleFromPoint(worker, max_x, max_y, rotate, {arc4random() % max_x, arc4random() % max_y}, true);
    }
    
    
    RectangleShapePtr RectangleShape::rectangleFromPoint(WorkerPtr worker, uint32_t max_x, uint32_t max_y, bool rotate, Point base_point, bool mutate_base_point)
    {
        uint32_t x1 = base_point.x;
        uint32_t y1 = base_point.y;
        
        uint32_t x2 = x1 + arc4random() % (_mult_base - 1) + 1;
        uint32_t y2 = y1 + arc4random() % (_mult_base - 1) + 1;
        
        Point x{x1, y1};
        Point y{x2, y2};
        
        RectangleShapePtr rect;
        if (rotate)
        {
            rect = std::make_shared<RectangleShape>(worker, x, y, arc4random() % 360, mutate_base_point);
        }
        else
        {
            rect = std::make_shared<RectangleShape>(worker, x, y, mutate_base_point);
        }
        
        rect->mutate(max_x, max_y);
        
        return rect;

    }
    
    
    ShapePtr RectangleShape::copy()
    {
        if (_rotate)
        {
            return std::make_shared<RectangleShape>(_worker, _p1, _p2, _angle, _mutate_base_point);
        }
        else
        {
            return std::make_shared<RectangleShape>(_worker, _p1, _p2, _mutate_base_point);
        }
    }
    
    
    void RectangleShape::mutate(uint32_t max_x, uint32_t max_y)
    {
        int32_t a = _rotate ? 3 : 2;
        int32_t b = _mutate_base_point ? 0 : 1;
        
        if (!_mutate_base_point)
            a--;
        
        int32_t point = (arc4random() % a) + b;
        
        switch (point)
        {
            case 0:
                _p1.x = _p1.x + int32_t(_worker->normRand() * _mult_half);
                _p1.y = _p1.y + int32_t(_worker->normRand() * _mult_half);
                break;
                
            case 1:
                _p2.x = _p1.x + int32_t(_worker->normRand()  * _mult_half);
                _p2.y = _p1.y + int32_t(_worker->normRand() * _mult_half);
                break;
                
            case 2:
                _angle += arc4random() % 180 - 90;
                break;
        }        
        
        _p1.clamp(0, max_x - 1, 0, max_y - 1);
        _p2.clamp(0, max_x - 1, 0, max_y - 1);
    }
    

    void RectangleShape::getBoundingRect(Point &min, Point &max)
    {
        if (_rotate)
        {
            Point a(_p1.x, _p2.y);
            Point b(_p2);
            Point c(_p2.x, _p1.y);
            
            a.rotate(_p1, _angle);
            b.rotate(_p1, _angle);
            c.rotate(_p1, _angle);
            
            min.x = std::min({a.x, b.x, c.x});
            min.y = std::min({a.y, b.y, c.y});
            
            max.x = std::max({a.x, b.x, c.x});
            max.y = std::max({a.y, b.y, c.y});
            
            min.clamp(0, _worker->width - 1, 0, _worker->height - 1);
            max.clamp(0, _worker->width - 1, 0, _worker->height - 1);
        }
        else
        {
            min = _p1;
            max = _p2;
        }
    }
    
    ShapePtr RectangleShape::scale(float x_factor, float y_factor)
    {
        if (_rotate)
        {
            return std::make_shared<RectangleShape>(_worker,
                                                    Point::scale(_p1, x_factor, y_factor),
                                                    Point::scale(_p2, x_factor, y_factor),
                                                    _angle,
                                                    _mutate_base_point);
        }
        else
        {
            return std::make_shared<RectangleShape>(_worker,
                                                    Point::scale(_p1, x_factor, y_factor),
                                                    Point::scale(_p2, x_factor, y_factor),
                                                    _mutate_base_point);
        }
    }
}
