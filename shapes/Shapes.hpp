//
//  Shapes.h
//  primitiveApp
//
//  Created by Nikita Kunevich on 2/5/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef Shapes_h
#define Shapes_h

#include "TriangleShape.hpp"
#include "RectangleShape.hpp"
#include "EllipseShape.hpp"
#include "PolygonShape.hpp"
#include "BezierShape.hpp"

#endif /* Shapes_h */
