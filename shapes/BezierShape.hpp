//
//  BezierShape.hpp
//  primitiveApp
//
//  Created by Anton Protko on 2/13/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef BezierShape_hpp
#define BezierShape_hpp

#include "Shape.hpp"

namespace primitive
{
    class BezierShape;
    typedef std::shared_ptr<BezierShape> BezierShapePtr;
    
    class BezierShape : public Shape
    {
    public:
        BezierShape(WorkerPtr worker, std::vector<Point> &points, bool mutate_base_point);
        
        static void setMultiplier(float mult);
        static BezierShapePtr randomBezier(WorkerPtr worker, uint32_t max_x, uint32_t max_y);
        static BezierShapePtr bezierFromPoint(WorkerPtr worker, uint32_t max_x, uint32_t max_y, Point point, bool mutate_base_point = true);
        
        virtual void draw(ImagePtr& target, RGBA32Color& color) override;
        virtual ShapePtr copy() override;
        virtual void mutate(uint32_t max_x, uint32_t max_y) override;
        
        virtual RGBA32Color computeColor(ImagePtr& target, ImagePtr& current, int alpha, bool& error) override;
        virtual double differencePartial(ImagePtr& target, ImagePtr& before, ImagePtr& after, double score) override;
        virtual void copyPixels(ImagePtr& target, ImagePtr& current) override;
        
        virtual void getBoundingRect(Point &min, Point &max) override;
        
        virtual ShapePtr scale(float x_factor, float y_factor) override;

    private:
        static float _mult;
        static int _mult_base;
        static int _mult_half;
        static int _mult_rand;


        std::vector<Point> _points;
    };
}

#endif /* BezierShape_hpp */
