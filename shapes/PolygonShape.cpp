//
//  PolygonShape.cpp
//  primitiveApp
//
//  Created by Anton Protko on 2/10/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#include "PolygonShape.hpp"
#include "Utility.hpp"
#include "Worker.hpp"

#include <cstdlib>
#include <cmath>

namespace primitive
{
    const int base_size = 40;
    const int base_half = base_size / 2;
    const int rand_size = 16;
    
    float PolygonShape::_mult = 1.f;
    int PolygonShape::_mult_base = base_size;
    int PolygonShape::_mult_half = base_half;
    int PolygonShape::_mult_rand = rand_size;
    
    PolygonShape::PolygonShape(WorkerPtr worker, std::vector<Point> &points, bool mutate_base_point)
    {
        _points = points;
        _worker = worker;
        
        _mutate_base_point = mutate_base_point;
    }
    
    void PolygonShape::setMultiplier(float mult) {
        _mult = Shape::_size_multiplier * mult;
        _mult_base = roundf(base_size * _mult);
        _mult_half = roundf(base_half * _mult);
        _mult_rand = roundf(rand_size * _mult);
    }
    
    void PolygonShape::draw(ImagePtr& target, RGBA32Color& color)
    {
        target->drawPolygon(_points, color);
    }
    
    
    PolygonShapePtr PolygonShape::randomPolygon(WorkerPtr worker, uint32_t max_x, uint32_t max_y)
    {
        return polygonFromPoint(worker, max_x, max_y, {arc4random() % max_x, arc4random() % max_y});
    }
    

    PolygonShapePtr PolygonShape::polygonFromPoint(WorkerPtr worker, uint32_t max_x, uint32_t max_y, Point point, bool mutate_base_point)
    {
        uint32_t x0 = point.x;
        uint32_t y0 = point.y;
        
        std::vector<Point> points;
        points.push_back(Point {x0, y0});
        
        int32_t order = 4;
        
        for (int32_t count = 1; count < order; count++)
        {
            uint32_t x = clamp<int32_t>(x0 + arc4random() % _mult_base - _mult_half, 0, max_x - 1);
            uint32_t y = clamp<int32_t>(y0 + arc4random() % _mult_base - _mult_half, 0, max_y - 1);
            
            points.push_back(Point {x, y});
        }
        
        PolygonShapePtr rect = std::make_shared<PolygonShape>(worker, points, mutate_base_point);
        rect->mutate(max_x, max_y);
        
        return rect;
    }
    
    
    ShapePtr PolygonShape::copy()
    {
        return std::make_shared<PolygonShape>(_worker, _points, _mutate_base_point);
    }

    
    void PolygonShape::mutate(uint32_t max_x, uint32_t max_y)
    {
        int32_t a = _mutate_base_point ? _points.size() : _points.size() - 1;
        int32_t b = _mutate_base_point ? 0 : 1;
        
        uint32_t index = (arc4random() % a) + b;
        
        _points[index].x = clamp<int32_t>(_points[index].x + int32_t(_worker->normRand() * _mult_rand), 0, max_x - 1);
        _points[index].y = clamp<int32_t>(_points[index].y + int32_t(_worker->normRand() * _mult_rand), 0, max_y - 1);
    }
    
    
    void PolygonShape::getBoundingRect(Point &min, Point &max)
    {
        auto yComp = [](const Point &a, const Point &b) { return a.y < b.y; };
        auto xComp = [](const Point &a, const Point &b) { return a.x < b.x; };
        
        uint32_t minY = std::min_element(_points.begin(), _points.end(), yComp)->y;
        uint32_t minX = std::min_element(_points.begin(), _points.end(), xComp)->x;
        uint32_t maxY = std::max_element(_points.begin(), _points.end(), yComp)->y;
        uint32_t maxX = std::max_element(_points.begin(), _points.end(), xComp)->x;
        
        min.x = minX;
        min.y = minY;
        
        max.x = maxX;
        max.y = maxY;
    }
    
    ShapePtr PolygonShape::scale(float x_factor, float y_factor)
    {
        std::vector<Point> scaled_points;
        
        for (auto &point : _points)
        {
            scaled_points.push_back(Point::scale(point, x_factor, y_factor));
        }
        
        return std::make_shared<PolygonShape>(_worker, scaled_points, _mutate_base_point);
    }
}
