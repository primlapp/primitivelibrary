//
//  Worker.cpp
//  primitiveApp
//
//  Created by Nikita Kunevich on 1/27/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#include <functional>

#include "Optimize.hpp"
#include "Worker.hpp"
#include "State.hpp"
#include "Utility.hpp"
#include "Primitivizer.hpp"

namespace primitive
{
    Worker::Worker(ImagePtr target) : _gen(), distrib(0.0, 1.0)
    {
//        this->_buffer = target->copy(true);
        this->width = target->width;
        this->height = target->height;
        this->_target = target;
        this->normRand = [this](){return distrib(_gen);};
    }
    
    void Worker::init(ImagePtr current, double score)
    {
        this->_buffer = current->copy(true);
        this->_current = current;
        this->_score = score;
    }
    
    double Worker::energy(ShapePtr shape, int alpha)
    {
        bool error = false;
        RGBA32Color color;

        do
        {
            color = shape->computeColor(this->_target, this->_buffer, alpha, error);
            if (error)
            {
                error = false;
                shape->mutate(width, height);
                color = shape->computeColor(this->_target, this->_buffer, alpha, error);
            }
        }
        while(error);
        
        shape->copyPixels(this->_buffer, this->_current);
        shape->draw(this->_buffer, color);
        return shape->differencePartial(this->_target, this->_current, this->_buffer, this->_score);
    }
    
    

    std::vector<StatePtr> Worker::bestHillClimbState(ShapeType shapeType , int alpha, int outer_count, int inner_count,int optimize_age, Point *base_point_ptr)
    {
        const int32_t best_states_num = Primitivizer::getInstance()->getGenerationParameters().WORKER_RETURN_COUNT;
        std::vector<StatePtr> best_states;
        
        for (int i = 0 ; i < outer_count; i++)
        {
            auto initial_state = (base_point_ptr == nullptr) ?
                bestRandomState(shapeType, alpha, inner_count) :
                stateFromPoint(shapeType, *base_point_ptr, alpha);
            
            std::vector<AnnealablePtr> hill_climb_states = HillClimb(std::dynamic_pointer_cast<Annealable>(initial_state), optimize_age);
            
            for (const auto &state : hill_climb_states)
            {
                StatePtr temp = std::dynamic_pointer_cast<State>(state);
                tryAddState(best_states, temp, best_states_num);
            }
        }
        
        return best_states;
    }


    StatePtr Worker::bestRandomState(ShapeType t, int alpha, int num)
    {
        double best_energy = DBL_MAX;
        StatePtr best_state;
        
        for (int i = 0; i < num; i++)
        {
            auto state = this->randomState(t, alpha);
            auto energy = state->energy();
            if (i == 0 || energy < best_energy)
            {
                best_energy = energy;
                best_state = state;
            }
        }
        return best_state;

    }
    
    
    StatePtr Worker::bestStateFromPoint(ShapeType t, int alpha, int num, Point base_point)
    {
        double best_energy = DBL_MAX;
        StatePtr best_state;
        
        for (int i = 0; i < num; i++)
        {
            auto state = this->stateFromPoint(t, base_point, alpha);
            auto energy = state->energy();
            if (i == 0 || energy < best_energy)
            {
                best_energy = energy;
                best_state = state;
            }
        }
        return best_state;
    }
    
    
    StatePtr Worker::stateFromPoint(ShapeType type, Point base_point, int alpha)
    {
        switch (type)
        {
            case ShapeType::Triangle:
                return StatePtr(new State(shared_from_this(),
                                          TriangleShapePtr(TriangleShape::triangleFromPoint(shared_from_this(), this->width, this->height, base_point)),
                                          alpha));
                
            case ShapeType::Rectangle:
                return StatePtr(new State(shared_from_this(),
                                          RectangleShapePtr(RectangleShape::rectangleFromPoint(shared_from_this(), this->width, this->height, false, base_point)),
                                          alpha));
                
            case ShapeType::Ellipse:
                return StatePtr(new State(shared_from_this(),
                                          EllipseShapePtr(EllipseShape::ellipseFromPoint(shared_from_this(), this->width, this->height, base_point)),
                                          alpha));
                
            case ShapeType::Polygon:
                return StatePtr(new State(shared_from_this(),
                                          PolygonShapePtr(PolygonShape::polygonFromPoint(shared_from_this(), this->width, this->height, base_point)),
                                          alpha));
                
            case ShapeType::Circle:
                return StatePtr(new State(shared_from_this(),
                                          EllipseShapePtr(EllipseShape::circleFromPoint(shared_from_this(), this->width, this->height, base_point)),
                                          alpha));
                
            case ShapeType::RotatedRectangle:
                return StatePtr(new State(shared_from_this(),
                                          RectangleShapePtr(RectangleShape::rectangleFromPoint(shared_from_this(), this->width, this->height, true, base_point)),
                                          alpha));
                
            case ShapeType::Bezier:
                return StatePtr(new State(shared_from_this(),
                                          BezierShapePtr(BezierShape::bezierFromPoint(shared_from_this(), this->width, this->height, base_point)),
                                          alpha));
                
                
            default:
                return randomState(ShapeType(arc4random() % 3 + 1), alpha);
        }
    }

    
    StatePtr Worker::randomState(ShapeType type, int alpha)
    {
        switch (type)
        {
            case ShapeType::Triangle:
                return StatePtr(new State(shared_from_this(),
                                          TriangleShapePtr(TriangleShape::randomTriangle(shared_from_this(), this->width, this->height)),
                                          alpha));
                
            case ShapeType::Rectangle:
                return StatePtr(new State(shared_from_this(),
                                          RectangleShapePtr(RectangleShape::randomRectangle(shared_from_this(), this->width, this->height, false)),
                                          alpha));
                
            case ShapeType::Ellipse:
                return StatePtr(new State(shared_from_this(),
                                          EllipseShapePtr(EllipseShape::randomEllipse(shared_from_this(), this->width, this->height)),
                                          alpha));
                
            case ShapeType::Polygon:
                return StatePtr(new State(shared_from_this(),
                                          PolygonShapePtr(PolygonShape::randomPolygon(shared_from_this(), this->width, this->height)),
                                          alpha));
                
            case ShapeType::Circle:
                return StatePtr(new State(shared_from_this(),
                                          EllipseShapePtr(EllipseShape::randomCircle(shared_from_this(), this->width, this->height)),
                                          alpha));
                
            case ShapeType::RotatedRectangle:
                return StatePtr(new State(shared_from_this(),
                                          RectangleShapePtr(RectangleShape::randomRectangle(shared_from_this(), this->width, this->height, true)),
                                          alpha));
                
            case ShapeType::Bezier:
                return StatePtr(new State(shared_from_this(),
                                          BezierShapePtr(BezierShape::randomBezier(shared_from_this(), this->width, this->height)),
                                          alpha));
                
                
            default:
                return randomState(ShapeType(arc4random() % 3 + 1), alpha);
        }
    }
}
