//
//  Optimize.hpp
//  primitiveApp
//
//  Created by Nikita Kunevich on 2/3/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef Optimize_hpp
#define Optimize_hpp

#include <memory>
#include <vector>

namespace primitive
{
    class Annealable;
    typedef std::shared_ptr<Annealable> AnnealablePtr;
    
    typedef std::pair<AnnealablePtr, double> AnnealablePair;
    
    class Annealable
    {
    public:
        virtual double energy()= 0;
        virtual AnnealablePtr doMove() = 0;
        virtual void undoMove(AnnealablePtr) = 0;
        virtual AnnealablePtr copy() = 0;
        
        virtual ~Annealable() { };
    };
    
    std::vector<AnnealablePtr> HillClimb(AnnealablePtr state, int maxAge);
    double PreAnneal(AnnealablePtr state, int iterations);
    AnnealablePtr Anneal(AnnealablePtr state, double max_temp, double min_temp, int steps);
}



#endif /* Optimize_hpp */
