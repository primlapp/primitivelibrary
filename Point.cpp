//
//  Point.cpp
//  primitiveApp
//
//  Created by Anton Protko on 2/10/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#include "Point.hpp"
#include "Utility.hpp"

#include <cmath>

namespace primitive
{
    Point::Point()
    {
        x = 0;
        y = 0;
    }
    
    Point::Point(int32_t x, int32_t y) : x(x), y(y) { }
    
    Point::Point(std::initializer_list<uint32_t> c)
    {
        x = (int32_t)(*c.begin());
        y = (int32_t)(*(c.begin() + 1));
    }
    
    Point::Point(std::initializer_list<int32_t> c)
    {
        x = *c.begin();
        y = *(c.begin() + 1);
    }
    
    
    Point& Point::operator += (const Point &other)
    {
        this->x += other.x;
        this->y += other.y;
        
        return *this;
    }
    
    
    Point& Point::operator -= (const Point &other)
    {
        this->x -= other.x;
        this->y -= other.y;
        
        return *this;
    }
    
    
    Point& Point::operator * (const float num)
    {
        x *= num;
        y *= num;
        
        return *this;
    }
    
    
    void Point::rotate(const Point &pivot, int32_t degrees)
    {
        float radeans = degrees * M_PI / 360.0f;
        this->rotate(pivot, radeans);
    }
    
    void Point::rotate(const Point &pivot, float radeans)
    {
        float current_angle = angle(pivot);
        float radius = distance(pivot);
        
        x = std::cos(current_angle + radeans) * radius;
        y = std::sin(current_angle + radeans) * radius;
        
        *this += pivot;
    }
    
    void Point::clamp(int32_t min_x, int32_t max_x, int32_t min_y, int32_t max_y)
    {
        this->x = primitive::clamp<int32_t>(this->x, min_x, max_x);
        this->y = primitive::clamp<int32_t>(this->y, min_y, max_y);
    }
    
    
    float Point::angle(const Point &point)
    {
        return std::atan2f(y - point.y, x - point.x);
    }
    
    
    float Point::distance(const Point &point)
    {
        return std::sqrtf(std::powf(point.x - x, 2) + std::powf(point.y - y, 2));
    }
    
        
    float Point::dot(Point &a, Point &b)
    {
        int32_t first[] = { a.x, a.y };
        int32_t second[] = { a.x, a.y };
        
        return std::inner_product(std::begin(first), std::end(first), std::begin(second), 0.0);
    }
    
    Point Point::scale(Point &point, float x_factor, float y_factor)
    {
        return Point(point.x * x_factor, point.y * y_factor);
    }
}
