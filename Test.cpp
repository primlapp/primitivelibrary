//
//  Test.cpp
//  primitiveApp
//
//  Created by Anton Protko on 2/2/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#include "Test.hpp"
#include "CGUtils.hpp"
#include "TriangleShapeBuilder.hpp"
#include "CircleShapeBuilder.hpp"
#include "SquareShapeBuilder.hpp"

#include <cstdlib>
#include <thread>


namespace primitive
{
    void tryAddPoints(std::queue<Point> &queue, int x, int y, int image_height, int image_width, char *busy)
    {
        Point points[8];
        points[0] = Point(x + 1, 	y);
        points[1] = Point(x + 1, 	y + 1);
        points[2] = Point(x, 		y + 1);
        points[3] = Point(x - 1, 	y + 1);
        points[4] = Point(x - 1, 	y);
        points[5] = Point(x - 1, 	y - 1);
        points[6] = Point(x, 		y - 1);
        points[7] = Point(x + 1, 	y - 1);
        
        for (int count = 0; count < 8; count++)
        {
            if ((points[count].x >= 0 && points[count].y >= 0)
                && (points[count].x < image_width && points[count].y < image_height))
            {
                int index = points[count].x * image_height + points[count].y;
                if (busy[index] == 0)
                {
                    queue.push(points[count]);
                    busy[index] = 1;
                }
            }
        }
    }
    
    
    void process(ImagePtr &original, ImagePtr &output, int spectrum, BuilderPtr &shapeBuilder)
    {
        int width = original->width;
        int height = original->height;
        
        char *busy = (char *)calloc(width * height, sizeof(char));
        
        //maybe move this settings to a ShapeBuilder
        const unsigned char possible_diff = 30;
        const int max_pixel_per_region = 500;
        const int min_pixel_per_region = 100;
        
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
            {
                int index = x * height + y;
                if (busy[index] == 1)
                {
                    continue;
                }
                else
                {
                    int current_region_pixels_count = 1;
                    unsigned char value = original->getPixelChannel(x, y, spectrum);
                    
                    busy[index] = 1;
                    
                    std::queue<Point> points_to_check;
                    
                    tryAddPoints(points_to_check, x, y, height, width, busy);
                    
                    shapeBuilder->init(x, y);
                    
                    while (!points_to_check.empty())
                    {
                        Point point = points_to_check.front();
                        points_to_check.pop();
                        
                        unsigned char value_to_check = original->getPixelChannel(point.x, point.y, spectrum);
                        int diff = std::abs((int)value_to_check - (int)value);
                        
                        if (diff <= possible_diff)
                        {
                            busy[point.x * height + point.y] = 1;
                            
                            current_region_pixels_count++;
                            if (current_region_pixels_count < max_pixel_per_region)
                            {
                                tryAddPoints(points_to_check, point.x, point.y, height, width, busy);
                            }
                            
                            value = (value_to_check + value * (current_region_pixels_count - 1)) / current_region_pixels_count;
                            
                            shapeBuilder->update(point);
                        }
                        else
                        {
                            busy[point.x * height + point.y] = 0;
                        }
                    }
                    
                    
                    if (current_region_pixels_count > min_pixel_per_region)
                    {
                        unsigned char shape_color[] = {0, 0, 0 };
                        shape_color[spectrum] = value;
                        
                        RGBA32Color color;
                        color.r = shape_color[0];
                        color.g = shape_color[1];
                        color.b = shape_color[2];
                        color.a = 255;
                        
                        shapeBuilder->draw(output, color, current_region_pixels_count);
                    }
                }
            }
        
        free(busy);
    }
    
    
//    unsigned char interpolatePixel(ImagePtr &image, int x, int y, int spectrum, bool original)
//    {
//        const int area = 3;
//        const int points_num = (2 * area + 1) * (2 * area + 1);
//        
//        Point points[points_num];
//        int point_num = 0;
//        
//        
//        for (int i = x - area; i <= x + area; i++)
//            for (int j = y - area; j <= y + area; j++)
//            {
//                points[point_num] = Point(i, j);
//                point_num++;
//            }
//        
//        int width = image.width();
//        int height = image.height();
//        
//        int extra_spectrum = (spectrum + 1) % 3;
//        
//        int count = 0;
//        int sum = 0;
//        for (int i = 0; i < points_num; i++)
//        {
//            if (points[i].x > 0 && points[i].y > 0 && points[i].x < width && points[i].y < height)
//            {
//                if (image(points[i].x, points[i].y, 0, extra_spectrum) == 0 || original)
//                {
//                    sum += image(points[i].x, points[i].y, 0, spectrum);
//                    count++;
//                }
//            }
//        }
//        
//        if (count > 0)
//        {
//            sum /= count;
//        }
//        else
//        {
//            // sum = rand() % 256;
//        }
//        
//        return sum;
//    }
    
    
    void postProcess(ImagePtr& image, ImagePtr& original)
    {
        const int allowed_diff = 78;
        
        int width = image->width;
        int height = image->height;
        
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
            {
                RGBA32Color pixel = image->getPixelAt(x, y);
                RGBA32Color originalPixel = original->getPixelAt(x, y);
                RGBA32Color newPixel = pixel;
                
                int32_t diff_r = std::abs(((int32_t)pixel.r) - originalPixel.r);
                int32_t diff_g = std::abs(((int32_t)pixel.g) - originalPixel.g);
                int32_t diff_b = std::abs(((int32_t)pixel.b) - originalPixel.b);
                
                // make some sort of convolution filter instead
                if (diff_r > allowed_diff)
                {
                    newPixel.r = originalPixel.r;
                }
                
                if (diff_g > allowed_diff)
                {
                    newPixel.g = originalPixel.g;
                }
                
                if (diff_b > allowed_diff)
                {
                    newPixel.b = originalPixel.b;
                }
                
#warning fix y axis invertion
                image->drawPoint(x, height - y - 1, newPixel);
            }
    }
    
    
    void processImage(ImagePtr &original, ImagePtr &out, ShapeType type)
    {
        int width = original->width;
        int height = original->height;
        
        CGContextRef r_context = CGEmptyContextFromRect(CGRectMake(0.0, 0.0, width, height));
        CGContextRef g_context = CGEmptyContextFromRect(CGRectMake(0.0, 0.0, width, height));
        CGContextRef b_context = CGEmptyContextFromRect(CGRectMake(0.0, 0.0, width, height));
        
        ImagePtr r = ImagePtr(new Image(r_context));
        ImagePtr g = ImagePtr(new Image(g_context));
        ImagePtr b = ImagePtr(new Image(b_context));
        
        BuilderPtr builder1 = createShapeBuilder(type);
        BuilderPtr builder2 = createShapeBuilder(type);
        BuilderPtr builder3 = createShapeBuilder(type);
        
        RGBA32Color bgColor = original->getAverageColor();
        r->fillRect(Point{0, 0}, Point{width, height}, bgColor);
        g->fillRect(Point{0, 0}, Point{width, height}, bgColor);
        b->fillRect(Point{0, 0}, Point{width, height}, bgColor);
        

#define THREADED
        
#ifdef THREADED
#warning check carefully with passing same reference to ImagePtr to threads
        std::thread t1(process, std::ref(original), std::ref(r), 0, std::ref(builder1));
        std::thread t2(process, std::ref(original), std::ref(g), 1, std::ref(builder2));
#endif
        
#ifndef THREADED
        process(original, r, 0, builder1);
        process(original, g, 1, builder2);
#endif
        process(original, b, 2, builder3);
        
#ifdef THREADED
        t1.join();
        t2.join();
#endif
        
        
#warning rewrite with joining buffers into one instead of rendering pixels
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
            {
                uint8_t r_color = r->getPixelAt(x, y).r;
                uint8_t g_color = g->getPixelAt(x, y).g;
                uint8_t b_color = b->getPixelAt(x, y).b;
                
                RGBA32Color color;
                color.r = r_color;
                color.g = g_color;
                color.b = b_color;
                color.a = 255;
                
                out->drawPoint(x, y, color);
            }
        
        postProcess(out, original);
    }
    
    
    BuilderPtr createShapeBuilder(ShapeType type)
    {
        switch (type)
        {
            case ShapeType::Triangle:
                return BuilderPtr(new TriangleShapeBuilder());
                
            case ShapeType::Circle:
                return BuilderPtr(new CircleShapeBuilder());
                
            case ShapeType::Square:
                return BuilderPtr(new SquareShapeBuilder());
                
            default:
                return BuilderPtr(new ShapeBuilder());
        }
    }
}
