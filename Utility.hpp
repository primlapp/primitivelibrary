//
//  Utility.hpp
//  primitiveApp
//
//  Created by maveric on 1/21/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef Utility_hpp
#define Utility_hpp

#include <algorithm>
#include <numeric>
#include <cmath>

#include "Image.hpp"
#include "State.hpp"

namespace primitive
{
    template <typename T>
    T clamp(const T& n, const T& lower, const T& upper)
    {
        return std::max(lower, std::min(n, upper));
    }    
    
    
    Point bezierPoint(float t, Point &p0, Point &p1, Point &p2, Point &p3)
    {
        float u = 1.0f - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;
        
        float x = p0.x * uuu;
        x += p1.x * 3.0f * uu * t;
        x += p2.x * 3.0f * u * tt;
        x += p3.x * ttt;
        
        float y = p0.y * uuu;
        y += p1.y * 3.0f * uu * t;
        y += p2.y * 3.0f * u * tt;
        y += p3.y * ttt;
        
        return Point(x, y);
    }
    
    
    std::vector<Point> bezierPath(Point &p0, Point &p1, Point &p2, Point &p3, uint32_t width, uint32_t height)
    {
        std::vector<Point> points;
        
        const float step = 0.01f;
        float time = 0.0f;
        
        do
        {
            Point current = bezierPoint(time, p0, p1, p2, p3);
            if (!(points.size() != 0 && points.back() == current))
            {
                if (current.x > 0 && current.x < width && current.y > 0 && current.y < height)
                {
                    points.push_back(current);
                }
            }
            
            time += step;
        }
        while (time < 1.0f);
        
        return points;
    }

    
    template <typename T>
    inline T lerp(T v0, T v1, float t)
    {
        return (1 - t) * v0 + t * v1;
    }
    
    
    template <typename T>
    bool tryAddState(std::vector<T> &states, T new_state, int32_t max_num)
    {
        auto comp = [](const T &a, const T &b) { return a->energy() < b->energy(); };
        
        auto iter = std::max_element(states.begin(), states.end(), comp);

        if (states.size() < max_num)
        {
            states.push_back(new_state);
            
            return true;
        }
        else if (iter != states.end())
        {
            if ((*iter)->energy() > new_state->energy())
            {
                states.erase(iter);
                states.push_back(new_state);
                
                return true;
            }
        }
        
        return false;
    }    
}


#endif /* Utility_hpp */
