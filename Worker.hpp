//
//  Worker.hpp
//  primitiveApp
//
//  Created by Nikita Kunevich on 1/27/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef Worker_hpp
#define Worker_hpp

#include <stdio.h>
#include <random>
#include <thread>

#include "Image.hpp"
#include "Shapes.hpp"

namespace primitive
{
    class State;
    typedef std::shared_ptr<State> StatePtr;
    
    typedef std::pair<StatePtr, double> StatePair;
    
    class Worker : public std::enable_shared_from_this<Worker>
    {
        std::mt19937 _gen;
        
        ImagePtr _buffer;
        ImagePtr _target;
        ImagePtr _current;
        double _score;
    public:
        std::normal_distribution<double> distrib;
        std::function<double(void)> normRand;
        
        uint32_t width;
        uint32_t height;
        
        Worker(ImagePtr target);
        double energy(ShapePtr shape, int alpha);

        void init(ImagePtr current, double score);
        std::vector<StatePtr> bestHillClimbState(ShapeType shapeType , int alpha, int outer_count, int inner_count,int optimize_age, Point *base_point_ptr);
        
    private:        
        StatePtr bestRandomState(ShapeType t, int a, int n);
        StatePtr bestStateFromPoint(ShapeType t, int a, int n, Point base_point);
        
        StatePtr randomState(ShapeType type, int alpha);
        StatePtr stateFromPoint(ShapeType type, Point base_point, int alpha);
    };
    
    typedef std::shared_ptr<Worker> WorkerPtr;
}
#endif /* Worker_hpp */
