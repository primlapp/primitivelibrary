//
//  TriangleShapeBuilder.hpp
//  primitiveApp
//
//  Created by Anton Protko on 2/2/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef TriangleShapeBuilder_hpp
#define TriangleShapeBuilder_hpp

#include "ShapeBuilder.hpp"
#include "Utility.hpp"

namespace primitive
{    
    class TriangleShapeBuilder : public ShapeBuilder
    {
    public:
        virtual void init(uint32_t x, uint32_t y) override
        {            
            p1 = Point(x, y);
            p2 = Point(-1, -1);
            p3 = Point(-1, -1);
            
            points_set = false;
        }
        
        virtual void update(Point &point) override
        {
            if (!points_set)
            {
                if (p2.x < 0)
                {
                    p2.x = point.x;
                    p2.y = point.y;
                }
                else if (p3.x < 0)
                {
                    p3.x = point.x;
                    p3.y = point.y;
                    points_set = true;
                    
                    d12 = Point::distance(p1, p2);
                    d13 = Point::distance(p1, p3);
                    d23 = Point::distance(p2, p3);
                }
            }
            else
            {
                float temp_1 = Point::distance(point, p1);
                float temp_2 = Point::distance(point, p2);
                float temp_3 = Point::distance(point, p3);
                
                if (temp_1 > temp_3)
                {
                    if (temp_2 > temp_3)
                    {
                        //1, 2
                        if (temp_1 > d13 && temp_2 > d23)
                        {
                            p3 = point;
                            d13 = temp_1;
                            d23 = temp_2;
                        }
                    }
                    else
                    {
                        //1, 3
                        if (temp_1 > d12 && temp_3 > d23)
                        {
                            p2 = point;
                            d12 = temp_1;
                            d23 = temp_3;
                        }
                    }
                }
                else
                {
                    if (temp_2 > temp_1)
                    {
                        //3, 2
                        if (temp_3 > d13 && temp_2 > d12)
                        {
                            p1 = point;
                            d13 = temp_3;
                            d12 = temp_2;
                        }
                    }
                    else
                    {
                        //3, 1
                        if (temp_1 > d12 && temp_3 > d23)
                        {
                            p2 = point;
                            d12 = temp_1;
                            d23 = temp_3;
                        }
                    }
                }
            }
        }
        
        virtual void draw(ImagePtr &target, RGBA32Color color, uint32_t pixel_count) override
        {
            float random_scale = randomFloat(1.5f, 2.0f);
            uint32_t width = target->width;
            uint32_t height = target->height;
            
            Point center((p1.x + p2.x + p3.x) / 3.0f, (p1.y + p2.y + p3.y) / 3.0f);
            p1.scale(random_scale, center);
            p2.scale(random_scale, center);
            p3.scale(random_scale, center);
            
#warning make appropriate triangle correction
#ifdef FIX_TRIANGLE
            const float min_distance_between_points = 10.0f;
            if (d12 < min_distance_between_points)
            {
                // scale is geometrically wrong
                float distance_scale = min_distance_between_points / d12;
                if (d23 > d13)
                {
                    //move p1
                    p1.scale(distance_scale, center);
                }
                else
                {
                    //move p2
                    p2.scale(distance_scale, center);
                }
            }
            
            if (d13 < min_distance_between_points)
            {
                float distance_scale = min_distance_between_points / d13;
                if (d23 > d12)
                {
                    //move p1
                    p1.scale(distance_scale, center);
                }
                else
                {
                    //move p3
                    p3.scale(distance_scale, center);
                }
            }
            
            if (d23 < min_distance_between_points)
            {
                float distance_scale = min_distance_between_points / d13;
                if (d12 > d13)
                {
                    //move p3
                    p3.scale(distance_scale, center);
                }
                else
                {
                    //move p1
                    p1.scale(distance_scale, center);
                }
            }
#endif
            
            
            p1.clamp(0, width, 0, height);
            p2.clamp(0, width, 0, height);
            p3.clamp(0, width, 0, height);
            
            target->drawTriangle(p1, p2, p3, color);
        }
        
        
    private:
        bool points_set;
        Point p1, p2, p3;
        float d12, d13, d23;
    };
}

#endif /* TriangleShapeBuilder_hpp */
