//
//  CircleShapeBuilder.hpp
//  primitiveApp
//
//  Created by Anton Protko on 2/2/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef CircleShapeBuilder_hpp
#define CircleShapeBuilder_hpp

#include "ShapeBuilder.hpp"
#include "Utility.hpp"

#define checkMin(a, b) if (a > b) a = b
#define checkMax(a, b) if (a < b) a = b

namespace primitive
{
    class CircleShapeBuilder : public ShapeBuilder
    {
    public:
        virtual void init(uint32_t x, uint32_t y) override
        {
            circle_center = Point(x, y);
            
            circle_max_x = x;
            circle_max_y = y;
            circle_min_x = x;
            circle_min_y = y;
        }
        
        virtual void update(Point &point) override
        {
            circle_center.x += point.x;
            circle_center.y += point.y;
            
            checkMin(circle_min_x, point.x);
            checkMin(circle_min_y, point.y);
            
            checkMax(circle_max_x, point.x);
            checkMax(circle_max_y, point.y);
        }
            
        virtual void draw(ImagePtr &target, RGBA32Color color, uint32_t pixel_count) override
        {
            float scale = randomFloat(1.5f, 3.0f);
                
            circle_center.x /= pixel_count;
            circle_center.y /= pixel_count;
                
            int average_x = ((circle_max_x - circle_center.x) + (circle_center.x - circle_min_x)) / 2;
            int average_y = ((circle_max_y - circle_center.y) + (circle_center.y - circle_min_y)) / 2;
            int radius = ((average_y + average_x) / 2) * scale;
                
            target->drawCircle(circle_center.x, circle_center.y, radius, color);
        }
            
    protected:
        Point circle_center;
        uint32_t circle_max_x, circle_max_y, circle_min_x, circle_min_y;
    };
            
}
            
#endif /* CircleShapeBuilder_hpp */
