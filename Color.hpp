//
//  Color.hpp
//  primitiveApp
//
//  Created by Nikita Kunevich on 1/21/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef Color_hpp
#define Color_hpp

#include <stdint.h>

namespace primitive {
    struct RGB24Color {
        uint8_t r;
        uint8_t g;
        uint8_t b;
    };
    
    struct RGBA32Color {
        uint8_t r;
        uint8_t g;
        uint8_t b;
        uint8_t a;
    };
}

#endif /* Color_hpp */
