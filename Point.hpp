//
//  Point.hpp
//  primitiveApp
//
//  Created by Anton Protko on 2/10/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef Point_hpp
#define Point_hpp

#include <stdint.h>
#include <memory>

namespace primitive
{
    struct Point
    {
        int32_t x;
        int32_t y;
        
        Point();
        Point(int32_t x, int32_t y);
        
        Point(std::initializer_list<uint32_t> c);
        Point(std::initializer_list<int32_t> c);
        
        Point& operator += (const Point &other);
        Point& operator -= (const Point &other);
        
        Point& operator * (const float num);
        
        void rotate(const Point &pivot, int32_t degrees);
        void rotate(const Point &pivot, float radeans);
        void clamp(int32_t min_x, int32_t max_x, int32_t min_y, int32_t max_y);
        float angle(const Point &point);
        float distance(const Point &point);
        
        
        static float dot(Point &a, Point &b);
        static Point scale(Point &point, float x_factor, float y_factor);
    };
    
    
    inline bool operator==(const Point& lhs, const Point& rhs){ return lhs.x == rhs.x && lhs.y == rhs.y;}
    inline bool operator!=(const Point& lhs, const Point& rhs){ return !(lhs == rhs); }
    inline Point operator + (const Point& lhs, const Point& rhs){ return Point{ lhs.x + rhs.x, lhs.y + rhs.y }; }
    inline Point operator - (const Point& lhs, const Point& rhs){ return Point{ lhs.x - rhs.x, lhs.y - rhs.y }; }

}

#endif /* Point_hpp */
