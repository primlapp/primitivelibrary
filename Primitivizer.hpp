//
//  Primitivizer.hpp
//  primitiveApp
//
//  Created by Nikita Kunevich on 1/21/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef Primitivizer_hpp
#define Primitivizer_hpp

#define real_out_size

#ifdef IOS
#include <CoreGraphics/CoreGraphics.h>
#include "CGUtils.hpp"
#include "ImageIOS.hpp"
#endif

#include <vector>
#include <memory>

#include "Image.hpp"
#include "Color.hpp"
#include "Shape.hpp"
#include "Worker.hpp"

namespace primitive {
    
    class Primitivizer;
    
    typedef std::shared_ptr<Primitivizer> PrimitivizerPtr;
    typedef void (^on_stop_block_t)(bool immediate);
    typedef void (^on_step_block_t)(float time);
    
//    typedef NS_ENUM(NSInteger, ImageFillType) {
//        ImageFillTypeAverage,
//        ImageFillTypeFinal,
//        ImageFillTypeNo
//    };
    
    enum ImageFillType {
        ImageFillTypeAverage,
        ImageFillTypeFinal,
        ImageFillTypeNo
    };
    
    struct GenerationParameters {
        int OUTER_ATTEMPT_COUNT;
        int OPTIMIZE_ATTEMPT_COUNT;
        int INNER_ATTEMPT_COUNT;
        int WORKER_RETURN_COUNT;
        int OPTIMIZE_RETURN_COUNT;
    };
    
    const GenerationParameters AutoPrecision = { 20, 10, 5, 3, 3 };
    const GenerationParameters ManualPrecision = { 20, 10, 5, 3, 1 };
    const GenerationParameters HighPrecision = { 100, 25, 16, 1, 1 };
    const GenerationParameters SuperHighPrecision = { 1000, 100, 16, 1, 1};
        
    class Primitivizer
    {
    private:
#ifdef real_out_size
        ImagePtr _current_scaled;
#endif
        
        ImagePtr _current;
        ImagePtr _target;
        std::vector<WorkerPtr> _workers;
        double _score;
        std::vector<ShapePtr> _shapes;
        std::vector<RGBA32Color> _colors;
        std::vector<double> _scores;
        
#ifdef real_out_size
        float _x_current_scaled_factor;
        float _y_current_scaled_factor;
#endif

        bool _stop_processing = false;
        bool _is_processing = false;
        on_stop_block_t _on_stop_block = NULL;

        static std::shared_ptr<Primitivizer> _instance;
        
        uint64_t _image_square;
        
    public:
#ifdef IOS
        static PrimitivizerPtr getInstance();
        Primitivizer();
        
        void Init(CGImageRef image_ref, uint32_t max_dimension);
        CGImageRef getCurrentImage();
        void fillCurrent(CGImageRef image);
#endif
        bool processImage(ShapeType shape, uint16_t shapeCount, on_step_block_t stepBlock);

        GenerationParameters genParams;

        void step(ShapeType shapeType, Point base_point);
        void step(ShapeType shapeType);
        
        void prepareToProcess(ImageFillType fillType);
        void stopProcessing();

        void stopProcessing(on_stop_block_t block);

        void setGenerationParams(GenerationParameters params);
        const GenerationParameters getGenerationParameters();
        
        void setShapesMultipliers(float mult);
        

    private:
        
        void clear();

        void add_shape(ShapePtr shape, int alpha);
        std::vector<StatePtr> runWorkers(ShapeType type, int alpha, int inner_count ,int optimize_max_age, int outer_count, Point *base_point_ptr);
        float energyForShape(ShapePtr& shape);
        ShapePtr getBestShape(ShapeType shapeType, uint16_t shapeCount);
        void step(ShapeType shapeType, int alpha, int repeat_count, Point *base_point_ptr, int shapes_to_add);
        
        static std::vector<StatePtr> runWorker(WorkerPtr worker, ShapeType shapeType, int alpha, int inner_count, int optimize_max_age, int outer_count, Point *base_point_ptr);
        static void generateShapes(ShapeType shapeType, std::vector<Shape*> result_array, int num, int count, int max_x, int max_y);
        static ShapePtr generateShapeWithWorker(WorkerPtr worker, ShapeType shapeType, int outer_attempt_count, int optimize_attempt_count, int inner_attempt_count);
    };
    
}


#endif /* Primitivizer_hpp */
