//
//  Image.hpp
//  primitiveApp
//
//  Created by Nikita Kunevich on 1/20/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef Image_hpp
#define Image_hpp

#ifdef IOS
#include <CoreGraphics/CoreGraphics.h>
#endif

#include <memory>
#include <vector>

#include "Color.hpp"
#include "Point.hpp"
#include "ShapeTypes.h"


namespace primitive
{
    
    class Image;
    typedef std::shared_ptr<Image> ImagePtr;
    
    class Image
    {
        
    public:
        uint32_t width;
        uint32_t height;
        RGBA32Color getAverageColor();
        
        double differenceFull(ImagePtr other);
                
        virtual ImagePtr copy(bool empty = false) = 0;
        virtual RGBA32Color getPixelAt(Point point) = 0;
        virtual RGBA32Color getPixelAt(uint32_t x, uint32_t y) = 0;
        virtual void setPixelAt(Point point, RGBA32Color color) = 0;
        virtual void fillRect(Point p1, Point p2, RGBA32Color col) = 0;
        
        virtual void drawTriangle(Point a, Point b, Point c, RGBA32Color color) = 0;
        virtual void drawRectangle(Point a, Point b, RGBA32Color color) = 0;
        virtual void drawEllipse(Point center, Point size, RGBA32Color color) = 0;
        virtual void drawPolygon(std::vector<Point> &points, RGBA32Color color) = 0;
        virtual void drawBezier(std::vector<Point> &points, RGBA32Color color) = 0;
        
        virtual ~Image() {};
        
    };
}
#endif /* Image_hpp */
