//
//  SquareShapeBuilder.hpp
//  primitiveApp
//
//  Created by Anton Protko on 2/3/17.
//  Copyright © 2017 Nikita Kunevich. All rights reserved.
//

#ifndef SquareShapeBuilder_hpp
#define SquareShapeBuilder_hpp

#include "ShapeBuilder.hpp"
#include "CircleShapeBuilder.hpp"


namespace primitive
{
    class SquareShapeBuilder : public CircleShapeBuilder
    {
        virtual void draw(ImagePtr &target, RGBA32Color color, uint32_t pixel_count)
        {
            float scale = randomFloat(1.5f, 3.0f);
            
            circle_center.x /= pixel_count;
            circle_center.y /= pixel_count;
            
            int average_x = ((circle_max_x - circle_center.x) + (circle_center.x - circle_min_x)) / 2;
            int average_y = ((circle_max_y - circle_center.y) + (circle_center.y - circle_min_y)) / 2;
            int radius = ((average_y + average_x) / 2) * scale;
            
//            target->drawCircle(circle_center.x, circle_center.y, radius, color);
            target->fillRect(Point(circle_center.x - radius, circle_center.y - radius), Point(circle_center.x + radius, circle_center.y + radius), color);
        };
    };
}

#endif /* SquareShapeBuilder_hpp */
